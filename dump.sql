--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6rc1
-- Dumped by pg_dump version 9.6rc1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: cube; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS cube WITH SCHEMA public;


--
-- Name: EXTENSION cube; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION cube IS 'data type for multidimensional cubes';


--
-- Name: earthdistance; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS earthdistance WITH SCHEMA public;


--
-- Name: EXTENSION earthdistance; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION earthdistance IS 'calculate great-circle distances on the surface of the Earth';


SET search_path = public, pg_catalog;

--
-- Name: password_hash; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN password_hash AS character(64);


ALTER DOMAIN password_hash OWNER TO postgres;

--
-- Name: stato_ordine; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN stato_ordine AS character varying
	CONSTRAINT stato_ordine_check CHECK ((((VALUE)::text = 'In attesa'::text) OR ((VALUE)::text = 'In preparazione'::text) OR ((VALUE)::text = 'Pronto'::text) OR ((VALUE)::text = 'In consegna'::text) OR ((VALUE)::text = 'Consegnato'::text)));


ALTER DOMAIN stato_ordine OWNER TO postgres;

--
-- Name: stato_preparazione; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN stato_preparazione AS character varying
	CONSTRAINT stato_piatto_check CHECK ((((VALUE)::text = 'Completato'::text) OR ((VALUE)::text = 'In preparazione'::text) OR ((VALUE)::text = 'In attesa'::text)));


ALTER DOMAIN stato_preparazione OWNER TO postgres;

--
-- Name: aggiorna_linee_preparazione(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION aggiorna_linee_preparazione() RETURNS trigger
    LANGUAGE plpgsql
    AS $$	DECLARE
    	query varchar; 
        nome linea_preparazione.nome%TYPE;
        id_linea linea_preparazione.id%TYPE;
    BEGIN
    	IF TG_OP = 'DELETE' THEN
        	id_linea = OLD.linea;
        ELSE
        	id_linea = NEW.linea;
       	END IF;
        
    	SELECT linea_preparazione.nome INTO nome FROM linea_preparazione WHERE id = id_linea AND giorno = current_date;

nome := nome || '_' || id_linea;
    	
        PERFORM * FROM pg_matviews WHERE matviewname = nome;
        IF NOT FOUND THEN
        	RAISE EXCEPTION 'La linea non ha una coda!';
        ELSE
        	execute('REFRESH MATERIALIZED VIEW "' || nome || '"');
        END IF;
        
		IF TG_OP = 'DELETE' THEN
        	RETURN OLD;
        ELSE
			RETURN NEW;
        END IF;
   	END;
$$;


ALTER FUNCTION public.aggiorna_linee_preparazione() OWNER TO postgres;

--
-- Name: aggiorna_stato_ordine(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION aggiorna_stato_ordine() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
    	IF NEW.stato = 'In preparazione' THEN
        	UPDATE ordine SET stato = 'In preparazione' WHERE id = NEW.ordine;
            RETURN NEW;
        END IF;
        
        IF NEW.stato = 'Completato' THEN
        	PERFORM * FROM preparazione WHERE ordine = NEW.ordine AND stato <> 'Completato';
            IF NOT FOUND THEN
            	UPDATE ordine SET stato = 'Pronto' WHERE id = NEW.ordine;
            ELSE 
            	UPDATE ordine SET stato = 'In preparazione' WHERE id = NEW.ordine;
            END IF;
            RETURN NEW;
        END IF;
    END;
$$;


ALTER FUNCTION public.aggiorna_stato_ordine() OWNER TO postgres;

--
-- Name: aggiorna_stato_ordine(integer, stato_ordine); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION aggiorna_stato_ordine(ordine integer, stato stato_ordine) RETURNS integer
    LANGUAGE plpgsql
    AS $_$    BEGIN
		PERFORM * FROM ordine WHERE id = $1;
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Ordine inesistente';
        END IF;
        
        UPDATE ordine SET stato = $2 WHERE id = $1;
        RETURN ordine;
    END;
$_$;


ALTER FUNCTION public.aggiorna_stato_ordine(ordine integer, stato stato_ordine) OWNER TO postgres;

--
-- Name: aggiorna_stato_preparazione(integer, stato_preparazione); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION aggiorna_stato_preparazione(preparazione integer, stato stato_preparazione) RETURNS integer
    LANGUAGE plpgsql
    AS $_$    BEGIN
		PERFORM * FROM preparazione WHERE id = $1;
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Ordine inesistente';
        END IF;
        
        UPDATE preparazione SET stato = $2 WHERE id = $1;
        RETURN preparazione;
    END;
$_$;


ALTER FUNCTION public.aggiorna_stato_preparazione(preparazione integer, stato stato_preparazione) OWNER TO postgres;

--
-- Name: aggiungi_preparazione_piatto(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION aggiungi_preparazione_piatto() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
        UPDATE piatto_del_giorno SET quantita = quantita - 1 WHERE piatto_del_giorno.piatto = NEW.piatto AND giorno = NEW.giorno AND piatto_del_giorno.giorno = NEW.giorno;
        RETURN NEW;
    END;
$$;


ALTER FUNCTION public.aggiungi_preparazione_piatto() OWNER TO postgres;

--
-- Name: annulla_ordine(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION annulla_ordine(ordine integer) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
    BEGIN
		PERFORM * FROM ordine WHERE id = $1 AND stato = 'In attesa';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Ordine non trovato o già in preparazione';
        END IF;
        DELETE FROM ordine WHERE id = $1;
        RETURN ordine;
    END;
$_$;


ALTER FUNCTION public.annulla_ordine(ordine integer) OWNER TO postgres;

--
-- Name: area_geografica_appartenenza(double precision, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION area_geografica_appartenenza(latitudine_indirizzo double precision, longitudine_indirizzo double precision) RETURNS integer
    LANGUAGE plpgsql
    AS $$	DECLARE
        indirizzo area_geografica.centro%TYPE;
        area area_geografica.id%TYPE;
    BEGIN
    	indirizzo := ll_to_earth(latitudine_indirizzo, longitudine_indirizzo);
        
        SELECT id, MIN(earth_distance(indirizzo, centro)) INTO area FROM area_geografica WHERE earth_distance(indirizzo, centro) <= raggio_metri GROUP BY id;
            
        IF NOT FOUND THEN
       		RAISE EXCEPTION 'L''indirizzo non appartiene a nessuna area geografica';
        END IF;
            
        RETURN area;
    END;
$$;


ALTER FUNCTION public.area_geografica_appartenenza(latitudine_indirizzo double precision, longitudine_indirizzo double precision) OWNER TO postgres;

--
-- Name: assegna_linea(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION assegna_linea() RETURNS trigger
    LANGUAGE plpgsql
    AS $$    DECLARE
    	linea linea_preparazione.id%TYPE;
        nomecat categoria.nome%TYPE;
        carico_linea piatto.tempo%TYPE;
        t_consegna area_geografica.tempo_consegna%TYPE;
        h_consegna ordine.ora_consegna%TYPE;
        t_piatto piatto.tempo%TYPE;
        t_max_piatto piatto.tempo%TYPE;
	BEGIN
    	SELECT categoria, tempo INTO nomecat, t_piatto FROM piatto WHERE id = NEW.piatto;
        
        SELECT MAX(tempo) INTO t_max_piatto 
        	FROM piatto JOIN piatto_del_giorno ON piatto.id = piatto_del_giorno.piatto
            WHERE piatto_del_giorno.giorno = NEW.giorno;

        SELECT ora_consegna, tempo_consegna INTO h_consegna, t_consegna FROM ordine 
        	JOIN area_geografica ON ordine.area_geografica = area_geografica.id
            WHERE ordine.id = NEW.ordine;
        
        SELECT linea_preparazione.id, CASE WHEN MAX(tempi_evasione.tempo) IS NULL THEN '0 minutes' ELSE MAX(tempi_evasione.tempo) END INTO linea, carico_linea
        FROM tempi_evasione RIGHT JOIN linea_preparazione ON tempi_evasione.linea = linea_preparazione.id
        WHERE linea_preparazione.giorno = NEW.giorno
        AND linea_preparazione.categoria = nomecat
        GROUP BY linea_preparazione.id
        HAVING CASE WHEN MAX(tempi_evasione.tempo) IS NULL THEN '0 minutes' ELSE MAX(tempi_evasione.tempo) END <= ALL
            (SELECT CASE WHEN MAX(tempi_evasione.tempo) IS NULL THEN '0 minutes' ELSE MAX(tempi_evasione.tempo) END
                FROM tempi_evasione RIGHT JOIN linea_preparazione ON tempi_evasione.linea = linea_preparazione.id
                WHERE linea_preparazione.giorno = NEW.giorno
                AND linea_preparazione.categoria = nomecat
                GROUP BY linea_preparazione.id);

        IF NOT FOUND OR (localtime + carico_linea + t_consegna + t_piatto - h_consegna > 2 * t_max_piatto) THEN 
        	SELECT crea_linea_preparazione(nomecat, 'linea_autogenerata') INTO linea;
        END IF;

        NEW.linea = linea;
        RETURN NEW;
    END;$$;


ALTER FUNCTION public.assegna_linea() OWNER TO postgres;

--
-- Name: autentica_cliente(character varying, password_hash); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION autentica_cliente(login character varying, password password_hash) RETURNS character varying
    LANGUAGE plpgsql
    AS $_$
BEGIN
PERFORM * FROM cliente WHERE cliente.login = $1 and cliente.password = $2;
IF NOT FOUND THEN
RAISE EXCEPTION 'Utente o password invalidi.';
END IF;
RETURN login;
END;
$_$;


ALTER FUNCTION public.autentica_cliente(login character varying, password password_hash) OWNER TO postgres;

--
-- Name: autentica_ristoratore(character varying, password_hash); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION autentica_ristoratore(login character varying, password password_hash) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$	BEGIN
    	PERFORM * FROM ristoratore WHERE ristoratore.login = $1 and ristoratore.password = $2;
        RETURN FOUND;
    END;
$_$;


ALTER FUNCTION public.autentica_ristoratore(login character varying, password password_hash) OWNER TO postgres;

--
-- Name: cambia_password_cliente(character varying, password_hash); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION cambia_password_cliente(login_in character varying, password_in password_hash) RETURNS character varying
    LANGUAGE plpgsql
    AS $_$BEGIN
PERFORM * FROM cliente WHERE login = $1;
IF NOT FOUND THEN
RAISE EXCEPTION 'Cliente inesistente';
END IF;
UPDATE cliente SET password = $2 WHERE login = $1;
RETURN login_in;
END;
$_$;


ALTER FUNCTION public.cambia_password_cliente(login_in character varying, password_in password_hash) OWNER TO postgres;

--
-- Name: cambia_password_ristoratore(character varying, password_hash); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION cambia_password_ristoratore(login_in character varying, password_in password_hash) RETURNS character varying
    LANGUAGE plpgsql
    AS $_$
BEGIN
PERFORM * FROM ristoratore WHERE login = $1;
IF NOT FOUND THEN
RAISE EXCEPTION 'Cliente inesistente';
END IF;
UPDATE ristoratore SET password = $2 WHERE login = $1;
RETURN login_in;
END;
$_$;


ALTER FUNCTION public.cambia_password_ristoratore(login_in character varying, password_in password_hash) OWNER TO postgres;

--
-- Name: cancella_preparazione_piatto(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION cancella_preparazione_piatto() RETURNS trigger
    LANGUAGE plpgsql
    AS $$	BEGIN
        IF OLD.stato <> 'In attesa' THEN
            RAISE EXCEPTION 'Piatto in preparazione o completato, impossibile annullare.';
        END IF;
        UPDATE piatto_del_giorno SET quantita = quantita + 1 WHERE piatto_del_giorno.piatto = OLD.piatto AND giorno = OLD.giorno AND piatto_del_giorno.giorno = OLD.giorno;
        RETURN OLD;
    END;
$$;


ALTER FUNCTION public.cancella_preparazione_piatto() OWNER TO postgres;

--
-- Name: controlla_stato_ordine(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION controlla_stato_ordine() RETURNS trigger
    LANGUAGE plpgsql
    AS $$	BEGIN
    	IF (NEW.stato = 'Pronto' OR NEW.stato = 'In consegna' OR NEW.stato = 'Consegnato') THEN
        	PERFORM * FROM preparazione WHERE ordine = NEW.id AND stato <> 'Completato';
            IF FOUND THEN
            	RAISE EXCEPTION 'Le preparazioni relative all''ordine non sono ancora state completate.';
            END IF;
        END IF;
        
        IF NEW.stato = 'In preparazione' THEN
        	PERFORM * FROM preparazione WHERE ordine = NEW.id AND stato <> 'In attesa';
            IF NOT FOUND THEN
            	RAISE EXCEPTION 'Nessun piatto dell''ordine è in preparazione o completato.';
            END IF;
        END IF;
        
        RETURN NEW;
    END;
$$;


ALTER FUNCTION public.controlla_stato_ordine() OWNER TO postgres;

--
-- Name: crea_area_geografica(character varying, double precision, double precision, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION crea_area_geografica(nome character varying, latitudine double precision, longitudine double precision, raggio_metri integer, tempo_consegna_minuti integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
	DECLARE 
    	c area_geografica.centro%TYPE;
        area area_geografica.id%TYPE;
        t area_geografica.tempo_consegna%TYPE;
    BEGIN
    	IF tempo_consegna_minuti < 0 THEN
        	RAISE EXCEPTION 'Il tempo di consegna non può essere negativo';
        END IF;
        t := tempo_consegna_minuti || ' minutes';
        
        IF raggio_metri < 0 THEN
        	RAISE EXCEPTION 'Il raggio dell''area non può essere negativo';
        END IF;
        
    	c := ll_to_earth(latitudine, longitudine);
        PERFORM * FROM area_geografica WHERE centro = c;
        IF FOUND THEN 
        	RAISE EXCEPTION 'Esiste già un''area geografica con lo stesso centro';
        END IF;

        INSERT INTO area_geografica(nome, centro, raggio_metri, tempo_consegna) VALUES (nome, c, raggio_metri, t) RETURNING id into area;
        RETURN area;
    END;                     
$$;


ALTER FUNCTION public.crea_area_geografica(nome character varying, latitudine double precision, longitudine double precision, raggio_metri integer, tempo_consegna_minuti integer) OWNER TO postgres;

--
-- Name: crea_categoria(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION crea_categoria(nome character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $_$
    BEGIN
    	PERFORM * FROM categoria WHERE categoria.nome = $1;
        IF FOUND THEN 
        	RAISE EXCEPTION 'Categoria già esistente';
       	END IF;
        
        INSERT INTO categoria(nome) VALUES(nome);
        RETURN nome;
    END;
$_$;


ALTER FUNCTION public.crea_categoria(nome character varying) OWNER TO postgres;

--
-- Name: crea_cliente(character varying, password_hash, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION crea_cliente(login character varying, password password_hash, nome character varying, cognome character varying, telefono character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $_$
    BEGIN
    	PERFORM * FROM cliente WHERE cliente.login = $1;
        IF FOUND THEN 
        	RAISE EXCEPTION 'Login già in uso';
       	END IF;
        
        INSERT INTO cliente(login, password, nome, cognome, telefono) VALUES(login, password, nome, cognome, telefono);
        RETURN login;
    END;
$_$;


ALTER FUNCTION public.crea_cliente(login character varying, password password_hash, nome character varying, cognome character varying, telefono character varying) OWNER TO postgres;

--
-- Name: crea_coda_linea(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION crea_coda_linea() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	BEGIN
    	PERFORM * FROM crea_coda_linea(NEW.id, NEW.giorno);
        RETURN NEW;
    END;
$$;


ALTER FUNCTION public.crea_coda_linea() OWNER TO postgres;

--
-- Name: crea_coda_linea(integer, date); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION crea_coda_linea(linea integer, giorno date) RETURNS void
    LANGUAGE plpgsql
    AS $_$
	DECLARE
    	query varchar;
        nome_linea linea_preparazione.nome%TYPE;
    BEGIN
    	SELECT linea_preparazione.nome INTO nome_linea FROM linea_preparazione WHERE linea_preparazione.id = $1 AND linea_preparazione.giorno = $2;
        IF NOT FOUND THEN
        	RAISE EXCEPTION 'Impossibile creare la coda, linea non trovata';
        END IF;

		nome_linea := nome_linea || '_' || linea;

        query := 'CREATE MATERIALIZED VIEW "' || nome_linea || '" AS (';
        query := query || 'SELECT preparazione.id AS preparazione ';
        query := query || 'FROM preparazione JOIN ordine ON preparazione.ordine = ordine.id ';
        query := query || 'WHERE preparazione.linea = ' || linea || ' AND preparazione.giorno = ''' || giorno || '''::date ';
        query := query || 'AND preparazione.stato <> ''Completato'' ';
        query := query || 'ORDER BY preparazione.stato DESC, ordine ASC)';
        execute(query);
    END;
$_$;


ALTER FUNCTION public.crea_coda_linea(linea integer, giorno date) OWNER TO postgres;

--
-- Name: crea_fattura(integer, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION crea_fattura(ordine integer, piva_cfisc character varying, indirizzo_fiscale character varying, nome_societa character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $_$    BEGIN
        PERFORM * FROM ordine WHERE id = $1;
        IF NOT FOUND THEN 
        	RAISE EXCEPTION 'Ordine inesistente';
       	END IF;
        
        INSERT INTO fattura(ordine, piva_cfisc, indirizzo_fiscale, nome_societa) VALUES
        	(ordine, piva_cfisc, indirizzo_fiscale, nome_societa);
            
        RETURN ordine;
    END;
$_$;


ALTER FUNCTION public.crea_fattura(ordine integer, piva_cfisc character varying, indirizzo_fiscale character varying, nome_societa character varying) OWNER TO postgres;

--
-- Name: crea_linea_preparazione(character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION crea_linea_preparazione(categoria character varying, nome character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $_$	DECLARE
    	created_id linea_preparazione.id%TYPE;
	BEGIN
    	PERFORM * FROM categoria WHERE categoria.nome = $1;
        IF NOT FOUND THEN
        	RAISE EXCEPTION 'Categoria inesistente';
        END IF;
        
        INSERT INTO linea_preparazione(categoria, nome) VALUES (categoria, nome) RETURNING id INTO created_id;
        RETURN created_id;
    END;
$_$;


ALTER FUNCTION public.crea_linea_preparazione(categoria character varying, nome character varying) OWNER TO postgres;

--
-- Name: crea_ordine(character varying, character varying, integer, time without time zone, integer[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION crea_ordine(cliente character varying, indirizzo_spedizione character varying, area_geografica integer, ora_consegna time without time zone, piatti integer[]) RETURNS integer
    LANGUAGE plpgsql
    AS $$
	DECLARE
    	ordine ordine.id%TYPE;
        piatto piatto_del_giorno.piatto%TYPE;
    BEGIN      
        INSERT INTO ordine(cliente, indirizzo_spedizione, ora_consegna, area_geografica, stato)
        	VALUES(cliente, indirizzo_spedizione, ora_consegna, area_geografica, 'In attesa') 
            RETURNING id INTO ordine;
            
        FOR piatto IN (SELECT piatto_del_giorno.piatto FROM unnest(piatti) JOIN piatto_del_giorno ON unnest = piatto_del_giorno.piatto JOIN piatto ON piatto_del_giorno.piatto = piatto.id WHERE piatto_del_giorno.giorno = current_date ORDER BY giorno DESC )
        LOOP
        	INSERT INTO preparazione(piatto, ordine) VALUES (piatto, ordine);
        END LOOP;
        
        RETURN ordine;
    END;    
$$;


ALTER FUNCTION public.crea_ordine(cliente character varying, indirizzo_spedizione character varying, area_geografica integer, ora_consegna time without time zone, piatti integer[]) OWNER TO postgres;

--
-- Name: crea_piatto(character varying, text, character varying, integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION crea_piatto(titolo character varying, descrizione text, path_foto character varying, complessita integer, tempo_minuti integer, categoria character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $_$    DECLARE
    	created_id piatto.id%TYPE;
        t piatto.tempo%TYPE;
    BEGIN
        PERFORM * FROM categoria WHERE nome = $6;
        IF NOT FOUND THEN 
        	RAISE EXCEPTION 'Categoria inesistente';
       	END IF;
        
        IF complessita < 1 OR complessita > 5 THEN
        	RAISE EXCEPTION 'La complessità dev''essere compresa fra 1 e 5';
        END IF;
        
        IF tempo_minuti < 0 THEN 
        	RAISE EXCEPTION 'Il tempo di preparazione non può essere negativo';
        END IF;
        t := tempo_minuti || ' minutes';
        
        INSERT INTO piatto(titolo, descrizione, path_foto, complessita, tempo, categoria) 
        	VALUES(titolo, descrizione, path_foto, complessita, t, categoria) RETURNING id INTO created_id;
            
        RETURN created_id;
    END;
$_$;


ALTER FUNCTION public.crea_piatto(titolo character varying, descrizione text, path_foto character varying, complessita integer, tempo_minuti integer, categoria character varying) OWNER TO postgres;

--
-- Name: crea_piatto_del_giorno(integer, numeric, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION crea_piatto_del_giorno(piatto integer, prezzo numeric, quantita integer) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
    BEGIN
        PERFORM * FROM piatto WHERE id = $1;
        IF NOT FOUND THEN 
        	RAISE EXCEPTION 'Piatto inesistente';
       	END IF;
        
        IF prezzo < 0 THEN
        	RAISE EXCEPTION 'Il prezzo non può essere negativo';
        END IF;
        
        IF quantita < 0 THEN 
        	RAISE EXCEPTION 'La quantità non può essere negativa';
        END IF;
        
        INSERT INTO piatto_del_giorno(piatto, prezzo, quantita) VALUES(piatto, prezzo, quantita);
        RETURN piatto;
    END;
$_$;


ALTER FUNCTION public.crea_piatto_del_giorno(piatto integer, prezzo numeric, quantita integer) OWNER TO postgres;

--
-- Name: crea_ristoratore(character varying, password_hash); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION crea_ristoratore(login character varying, password password_hash) RETURNS character varying
    LANGUAGE plpgsql
    AS $_$
	BEGIN
    	PERFORM * FROM ristoratore WHERE ristoratore.login = $1;
        IF FOUND THEN
        	RAISE EXCEPTION 'Login già in uso';
        END IF;
        
        INSERT INTO ristoratore(login, password) VALUES (login, password);
        RETURN login;
    END;
$_$;


ALTER FUNCTION public.crea_ristoratore(login character varying, password password_hash) OWNER TO postgres;

--
-- Name: inizia_nuova_giornata(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION inizia_nuova_giornata() RETURNS void
    LANGUAGE plpgsql
    AS $$	DECLARE
    	linea pg_matviews.matviewname%TYPE;
    BEGIN
    	FOR linea IN (SELECT matviewname FROM pg_matviews WHERE schemaname='public')
        LOOP
        	execute('DROP MATERIALIZED VIEW "' || linea || '"');
        END LOOP;
        ALTER SEQUENCE linea_preparazione_id_seq RESTART WITH 1;       
    END;
$$;


ALTER FUNCTION public.inizia_nuova_giornata() OWNER TO postgres;

--
-- Name: stima_consegna_ordine(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION stima_consegna_ordine(ordine integer) RETURNS time without time zone
    LANGUAGE plpgsql
    AS $_$	DECLARE
    	t1 interval;
        t2 interval;
        s ordine.stato%TYPE;
	BEGIN
    	SELECT stato INTO s FROM ordine WHERE ordine.id = $1;
        IF NOT FOUND THEN
        	RAISE EXCEPTION 'Ordine inesistente';
        END IF;
        
        IF s = 'Consegnato' THEN
        	RAISE EXCEPTION 'Ordine già consegnato';
        END IF;
        
        SELECT tempo_consegna INTO t1 FROM area_geografica JOIN ordine ON area_geografica.id = ordine.area_geografica;
        IF s = 'Pronto' THEN
        	RETURN localtime + t1;
        END IF;
        
        SELECT MAX(tempo) INTO t2 FROM tempi_evasione WHERE tempi_evasione.ordine = $1;
    	RETURN localtime + t1 + t2;
    END;
$_$;


ALTER FUNCTION public.stima_consegna_ordine(ordine integer) OWNER TO postgres;

--
-- Name: un_piatto_per_volta(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION un_piatto_per_volta() RETURNS trigger
    LANGUAGE plpgsql
    AS $$	BEGIN
    	IF (NEW.stato = 'In preparazione') THEN
        	PERFORM * FROM preparazione WHERE linea = NEW.linea AND stato = 'In preparazione' AND giorno = NEW.giorno;
            IF FOUND THEN
            	RAISE EXCEPTION 'Un piatto \`e gi\`a in preparazione su questa linea';
            END IF;
        END IF;
        
        RETURN NEW;
    END;
$$;


ALTER FUNCTION public.un_piatto_per_volta() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: area_geografica; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE area_geografica (
    raggio_metri integer NOT NULL,
    centro earth NOT NULL,
    nome character varying NOT NULL,
    tempo_consegna interval,
    id integer NOT NULL,
    CONSTRAINT area_geografica_raggio_check CHECK (((raggio_metri)::double precision > (0)::double precision)),
    CONSTRAINT area_geografica_tempo_consegna_check CHECK ((NOT (tempo_consegna < '00:00:00'::interval)))
);


ALTER TABLE area_geografica OWNER TO postgres;

--
-- Name: area_geografica_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE area_geografica_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE area_geografica_id_seq OWNER TO postgres;

--
-- Name: area_geografica_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE area_geografica_id_seq OWNED BY area_geografica.id;


--
-- Name: categoria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE categoria (
    nome character varying NOT NULL
);


ALTER TABLE categoria OWNER TO postgres;

--
-- Name: cliente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cliente (
    login character varying NOT NULL,
    password password_hash NOT NULL,
    nome character varying NOT NULL,
    cognome character varying NOT NULL,
    telefono character varying NOT NULL
);


ALTER TABLE cliente OWNER TO postgres;

--
-- Name: fattura; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE fattura (
    ordine integer NOT NULL,
    piva_cfisc character varying(16) NOT NULL,
    indirizzo_fiscale character varying(30) NOT NULL,
    nome_societa character varying(30)
);


ALTER TABLE fattura OWNER TO postgres;

--
-- Name: linea_preparazione; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE linea_preparazione (
    id integer NOT NULL,
    categoria character varying NOT NULL,
    giorno date DEFAULT ('now'::text)::date NOT NULL,
    nome character varying(100) NOT NULL
);


ALTER TABLE linea_preparazione OWNER TO postgres;

--
-- Name: linea_preparazione_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE linea_preparazione_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE linea_preparazione_id_seq OWNER TO postgres;

--
-- Name: linea_preparazione_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE linea_preparazione_id_seq OWNED BY linea_preparazione.id;


--
-- Name: ordine; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE ordine (
    id integer NOT NULL,
    cliente character varying NOT NULL,
    indirizzo_spedizione character varying(50) NOT NULL,
    ora_consegna time without time zone,
    area_geografica integer NOT NULL,
    stato stato_ordine NOT NULL
);


ALTER TABLE ordine OWNER TO postgres;

--
-- Name: ordine_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ordine_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ordine_id_seq OWNER TO postgres;

--
-- Name: ordine_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE ordine_id_seq OWNED BY ordine.id;


--
-- Name: piatto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE piatto (
    id integer NOT NULL,
    titolo character varying NOT NULL,
    descrizione text NOT NULL,
    complessita integer NOT NULL,
    path_foto character varying(50),
    categoria character varying NOT NULL,
    tempo interval,
    CONSTRAINT piatto_complessita_check CHECK (((complessita <= 5) AND (complessita >= 1))),
    CONSTRAINT piatto_tempo_check CHECK ((tempo >= '00:00:00'::interval))
);


ALTER TABLE piatto OWNER TO postgres;

--
-- Name: piatto_del_giorno; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE piatto_del_giorno (
    piatto integer NOT NULL,
    prezzo numeric(6,2) NOT NULL,
    quantita integer NOT NULL,
    giorno date DEFAULT ('now'::text)::date NOT NULL,
    CONSTRAINT piatto_del_giorno_prezzo_check CHECK ((prezzo >= (0)::numeric)),
    CONSTRAINT piatto_del_giorno_quantita_check CHECK ((quantita >= 0))
);


ALTER TABLE piatto_del_giorno OWNER TO postgres;

--
-- Name: piatto_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE piatto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE piatto_id_seq OWNER TO postgres;

--
-- Name: piatto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE piatto_id_seq OWNED BY piatto.id;


--
-- Name: preparazione; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE preparazione (
    id integer NOT NULL,
    linea integer NOT NULL,
    piatto integer NOT NULL,
    ordine integer NOT NULL,
    giorno date DEFAULT ('now'::text)::date NOT NULL,
    stato stato_preparazione DEFAULT 'In attesa'::character varying
);


ALTER TABLE preparazione OWNER TO postgres;

--
-- Name: prepara_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE prepara_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE prepara_id_seq OWNER TO postgres;

--
-- Name: prepara_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE prepara_id_seq OWNED BY preparazione.id;


--
-- Name: ristoratore; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE ristoratore (
    login character varying NOT NULL,
    password password_hash
);


ALTER TABLE ristoratore OWNER TO postgres;

--
-- Name: statistiche_linee; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW statistiche_linee AS
 SELECT preparazione.linea,
    preparazione.giorno,
    count(preparazione.piatto) AS num_piatto,
    sum(piatto.tempo) AS tempo_lavoro,
    count(DISTINCT ordine.cliente) AS clienti_serviti,
    avg(piatto_del_giorno.prezzo) AS prezzo_medio_piatto
   FROM (((preparazione
     JOIN piatto ON ((preparazione.piatto = piatto.id)))
     JOIN ordine ON ((preparazione.ordine = ordine.id)))
     JOIN piatto_del_giorno ON (((preparazione.piatto = piatto_del_giorno.piatto) AND (preparazione.giorno = piatto_del_giorno.giorno))))
  GROUP BY preparazione.linea, preparazione.giorno;


ALTER TABLE statistiche_linee OWNER TO postgres;

--
-- Name: tempi_evasione; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW tempi_evasione AS
 WITH tempo_ordine(ordine, linea, tempo) AS (
         SELECT preparazione.ordine,
            preparazione.linea,
            sum(piatto.tempo) AS sum
           FROM (preparazione
             JOIN piatto ON ((preparazione.piatto = piatto.id)))
          WHERE ((preparazione.ordine IN ( SELECT DISTINCT preparazione_1.ordine
                   FROM preparazione preparazione_1
                  WHERE ((preparazione_1.stato)::text <> 'Completato'::text))) AND ((preparazione.stato)::text <> 'Completato'::text) AND (preparazione.giorno = ('now'::text)::date))
          GROUP BY preparazione.ordine, preparazione.linea
        )
 SELECT t2.ordine,
    t2.linea,
    sum(t1.tempo) AS tempo
   FROM (tempo_ordine t1
     JOIN tempo_ordine t2 ON ((t1.linea = t2.linea)))
  WHERE (t1.ordine <= t2.ordine)
  GROUP BY t2.ordine, t2.linea;


ALTER TABLE tempi_evasione OWNER TO postgres;

--
-- Name: area_geografica id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY area_geografica ALTER COLUMN id SET DEFAULT nextval('area_geografica_id_seq'::regclass);


--
-- Name: linea_preparazione id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY linea_preparazione ALTER COLUMN id SET DEFAULT nextval('linea_preparazione_id_seq'::regclass);


--
-- Name: ordine id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ordine ALTER COLUMN id SET DEFAULT nextval('ordine_id_seq'::regclass);


--
-- Name: piatto id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY piatto ALTER COLUMN id SET DEFAULT nextval('piatto_id_seq'::regclass);


--
-- Name: preparazione id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY preparazione ALTER COLUMN id SET DEFAULT nextval('prepara_id_seq'::regclass);


--
-- Data for Name: area_geografica; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY area_geografica (raggio_metri, centro, nome, tempo_consegna, id) FROM stdin;
2000	(4411546.4791492391, 694602.11273983365, 4553769.0546960449)	parabiago	00:15:00	4
3000	(4408798.9628935494, 691522.81746857078, 4556897.5118970023)	legnano	00:15:00	5
2000	(4403610.0749871153, 716587.1779505054, 4558042.1411054228)	lissone	00:15:00	6
10000	(4390216.5398589214, 702033.83397924027, 4573201.7520902772)	como	00:15:00	7
2000	(4416393.9004542455, 716372.63050998375, 4545690.5093187699)	via comelico milano	00:10:00	8
\.


--
-- Name: area_geografica_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('area_geografica_id_seq', 8, true);


--
-- Data for Name: categoria; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY categoria (nome) FROM stdin;
primo
dolce
secondo
antipasto
contorno
\.


--
-- Data for Name: cliente; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cliente (login, password, nome, cognome, telefono) FROM stdin;
luca	d70f47790f689414789eeff231703429c7f88a10210775906460edbf38589d90	lcua	luca	12312
bea	b133a0c0e9bee3be20163d2ad31d6248db292aa6dcb1ee087a2aa50e0fc75ae2	bea	bea@gmial.com	+393403744999
maxpoggi	24d1455b64b01b556691fc9c5ae39350a1e2a97b2b0e7eb3ba06b20612d5a18a	Massimiliano	Poggi	3477104906
maybetomorrow	c5815ad83db92a3f419835828264eb861f1de630545a3ca9085ae277ccbe2452	maybe	tomorrow	1234567890
\.


--
-- Data for Name: fattura; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY fattura (ordine, piva_cfisc, indirizzo_fiscale, nome_societa) FROM stdin;
13	codice fiscale	indirizzo fiscale	intestazione
14	vcclcu94b19d198b	VIa damiano chiesa 13	Vecchi gian franco s.a.s.
16	vcclcu94b19d198b	VIa damiano chiesa 13	Vecchi gian franco s.a.s.
21	maybetom	tomorrow maybe	maybe tomorrow
22	abc	prova45	prova123
23	beacodicefiscale	via del luca 17	
\.


--
-- Data for Name: linea_preparazione; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY linea_preparazione (id, categoria, giorno, nome) FROM stdin;
1	secondo	2016-09-21	linea_autogenerata
2	primo	2016-09-21	linea_autogenerata
3	dolce	2016-09-21	linea_autogenerata
4	contorno	2016-09-21	linea_autogenerata
5	primo	2016-09-21	linea di prova
6	primo	2016-09-21	provalinea
7	contorno	2016-09-21	linea_autogenerata
8	contorno	2016-09-21	linea_autogenerata
9	primo	2016-09-21	la mia linea di preparazione con un nome davvero lungo
\.


--
-- Name: linea_preparazione_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('linea_preparazione_id_seq', 1, false);


--
-- Data for Name: ordine; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ordine (id, cliente, indirizzo_spedizione, ora_consegna, area_geografica, stato) FROM stdin;
13	bea	legnano	15:30:00	5	Consegnato
14	bea	parabiago	11:00:00	4	Consegnato
31	bea	via del luca 17	22:45:00	5	Consegnato
15	bea	4 novembre parabiago 20015	12:30:00	4	Consegnato
16	bea	via damiano chiesa 13 parabiago 20015	17:35:00	4	Consegnato
17	bea	parabiago	19:00:00	4	Consegnato
19	maybetomorrow	lissone	20:00:00	6	Consegnato
21	maybetomorrow	via comelico milano	21:30:00	8	Consegnato
22	maybetomorrow	via comelico milano	21:45:00	8	Consegnato
23	bea	via comelico 39 milano italia	22:15:00	8	Consegnato
32	bea	via del luca 17	22:45:00	5	Consegnato
40	bea	via del luca 17	22:45:00	5	Consegnato
24	bea	via comelico 39 milano	22:20:00	8	Consegnato
25	bea	via baldironi lissone	22:45:00	6	Consegnato
26	bea	via baldironi lissone	22:10:00	6	Consegnato
28	bea	via del luca 17	22:20:00	4	Consegnato
\.


--
-- Name: ordine_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ordine_id_seq', 40, true);


--
-- Data for Name: piatto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY piatto (id, titolo, descrizione, complessita, path_foto, categoria, tempo) FROM stdin;
1	Costata alla brace	Costata alle verdure	3	img/c3ce361f362a88254fbfd486f83a55b6f606dc1f	secondo	00:20:00
2	Spaghetti ER	Gli spaghetti al nero d'entità, specialità della casa.	2	img/870ebafeafcf13dcd43a88524e0aba595a3fef50	primo	00:12:00
3	Tiramisù di knam	Il classico dolce rivisitato al caffè e fragole.	4	img/9da46600f3728cfd3e3698e74bd5c98f3fd1855c	dolce	00:15:00
4	Patate al rosmarino	Un classico da asporto, al burro e rosmarino\t\t\t\t\t	1	img/e0b21de7587640dc70e97fa1339ae52592eeee66	contorno	00:40:00
\.


--
-- Data for Name: piatto_del_giorno; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY piatto_del_giorno (piatto, prezzo, quantita, giorno) FROM stdin;
4	4.00	30	2016-09-20
1	15.00	60	2016-09-20
2	7.00	100	2016-09-20
3	12.00	120	2016-09-20
4	4.00	22	2016-09-21
1	15.00	47	2016-09-21
3	12.00	61	2016-09-21
2	7.00	77	2016-09-21
\.


--
-- Name: piatto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('piatto_id_seq', 4, true);


--
-- Name: prepara_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('prepara_id_seq', 86, true);


--
-- Data for Name: preparazione; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY preparazione (id, linea, piatto, ordine, giorno, stato) FROM stdin;
13	1	1	13	2016-09-21	Completato
14	2	2	13	2016-09-21	Completato
15	3	3	13	2016-09-21	Completato
16	1	1	14	2016-09-21	Completato
17	2	2	14	2016-09-21	Completato
18	2	2	14	2016-09-21	Completato
24	4	4	15	2016-09-21	Completato
19	2	2	15	2016-09-21	Completato
72	5	2	31	2016-09-21	Completato
73	5	2	31	2016-09-21	Completato
20	1	1	15	2016-09-21	Completato
21	1	1	15	2016-09-21	Completato
22	3	3	15	2016-09-21	Completato
23	3	3	15	2016-09-21	Completato
25	1	1	16	2016-09-21	Completato
26	2	2	16	2016-09-21	Completato
28	2	2	17	2016-09-21	Completato
27	4	4	17	2016-09-21	Completato
33	2	2	19	2016-09-21	Completato
34	1	1	19	2016-09-21	Completato
35	4	4	19	2016-09-21	Completato
43	1	1	21	2016-09-21	Completato
44	1	1	21	2016-09-21	Completato
42	3	3	21	2016-09-21	Completato
47	5	2	21	2016-09-21	Completato
45	4	4	21	2016-09-21	Completato
46	7	4	21	2016-09-21	Completato
48	8	4	21	2016-09-21	Completato
51	1	1	22	2016-09-21	Completato
49	3	3	22	2016-09-21	Completato
50	3	3	22	2016-09-21	Completato
52	5	2	23	2016-09-21	Completato
54	3	3	23	2016-09-21	Completato
55	1	1	23	2016-09-21	Completato
53	8	4	23	2016-09-21	Completato
74	5	2	32	2016-09-21	Completato
75	5	2	32	2016-09-21	Completato
85	2	2	40	2016-09-21	Completato
86	5	2	40	2016-09-21	Completato
59	5	2	24	2016-09-21	Completato
56	5	2	24	2016-09-21	Completato
57	8	4	24	2016-09-21	Completato
58	3	3	24	2016-09-21	Completato
60	1	1	25	2016-09-21	Completato
61	1	1	25	2016-09-21	Completato
62	5	2	25	2016-09-21	Completato
63	5	2	25	2016-09-21	Completato
64	5	2	26	2016-09-21	Completato
65	5	2	26	2016-09-21	Completato
66	1	1	26	2016-09-21	Completato
67	3	3	26	2016-09-21	Completato
68	5	2	28	2016-09-21	Completato
69	5	2	28	2016-09-21	Completato
\.


--
-- Data for Name: ristoratore; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ristoratore (login, password) FROM stdin;
admin	8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918
\.


--
-- Name: area_geografica area_geografica_centro_check; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY area_geografica
    ADD CONSTRAINT area_geografica_centro_check UNIQUE (centro);


--
-- Name: area_geografica area_geografica_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY area_geografica
    ADD CONSTRAINT area_geografica_pkey PRIMARY KEY (id);


--
-- Name: categoria categoria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categoria
    ADD CONSTRAINT categoria_pkey PRIMARY KEY (nome);


--
-- Name: cliente cliente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cliente
    ADD CONSTRAINT cliente_pkey PRIMARY KEY (login);


--
-- Name: fattura fattura_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY fattura
    ADD CONSTRAINT fattura_pkey PRIMARY KEY (ordine);


--
-- Name: linea_preparazione linea_preparazione_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY linea_preparazione
    ADD CONSTRAINT linea_preparazione_pkey PRIMARY KEY (id, giorno);


--
-- Name: ordine ordine_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ordine
    ADD CONSTRAINT ordine_pkey PRIMARY KEY (id);


--
-- Name: piatto_del_giorno piatto_del_giorno_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY piatto_del_giorno
    ADD CONSTRAINT piatto_del_giorno_pkey PRIMARY KEY (piatto, giorno);


--
-- Name: piatto piatto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY piatto
    ADD CONSTRAINT piatto_pkey PRIMARY KEY (id);


--
-- Name: preparazione prepara_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY preparazione
    ADD CONSTRAINT prepara_pkey PRIMARY KEY (id);


--
-- Name: ristoratore ristoratore_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ristoratore
    ADD CONSTRAINT ristoratore_pkey PRIMARY KEY (login);


--
-- Name: preparazione aggiorna_disponibilita_piatto; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER aggiorna_disponibilita_piatto AFTER INSERT ON preparazione FOR EACH ROW EXECUTE PROCEDURE aggiungi_preparazione_piatto();


--
-- Name: preparazione aggiorna_linee_preparazione; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER aggiorna_linee_preparazione AFTER INSERT OR DELETE OR UPDATE ON preparazione FOR EACH ROW EXECUTE PROCEDURE aggiorna_linee_preparazione();


--
-- Name: preparazione aggiorna_stato_ordine; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER aggiorna_stato_ordine AFTER UPDATE ON preparazione FOR EACH ROW EXECUTE PROCEDURE aggiorna_stato_ordine();


--
-- Name: preparazione assegna_linea; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER assegna_linea BEFORE INSERT ON preparazione FOR EACH ROW EXECUTE PROCEDURE assegna_linea();


--
-- Name: preparazione cancellazione_piatto; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER cancellazione_piatto BEFORE DELETE ON preparazione FOR EACH ROW EXECUTE PROCEDURE cancella_preparazione_piatto();


--
-- Name: ordine check_stato_ordine; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER check_stato_ordine BEFORE UPDATE ON ordine FOR EACH ROW EXECUTE PROCEDURE controlla_stato_ordine();


--
-- Name: linea_preparazione crea_coda_linea; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER crea_coda_linea AFTER INSERT ON linea_preparazione FOR EACH ROW EXECUTE PROCEDURE crea_coda_linea();


--
-- Name: preparazione un_piatto_per_volta; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER un_piatto_per_volta BEFORE UPDATE ON preparazione FOR EACH ROW EXECUTE PROCEDURE un_piatto_per_volta();


--
-- Name: fattura fattura_ordine_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY fattura
    ADD CONSTRAINT fattura_ordine_fkey FOREIGN KEY (ordine) REFERENCES ordine(id) ON DELETE CASCADE;


--
-- Name: linea_preparazione linea_preparazione_categoria_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY linea_preparazione
    ADD CONSTRAINT linea_preparazione_categoria_fkey FOREIGN KEY (categoria) REFERENCES categoria(nome);


--
-- Name: ordine ordine_area_geografica_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ordine
    ADD CONSTRAINT ordine_area_geografica_fkey FOREIGN KEY (area_geografica) REFERENCES area_geografica(id);


--
-- Name: ordine ordine_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ordine
    ADD CONSTRAINT ordine_cliente_fkey FOREIGN KEY (cliente) REFERENCES cliente(login);


--
-- Name: piatto piatto_categoria_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY piatto
    ADD CONSTRAINT piatto_categoria_fkey FOREIGN KEY (categoria) REFERENCES categoria(nome);


--
-- Name: piatto_del_giorno piatto_del_giorno_piatto_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY piatto_del_giorno
    ADD CONSTRAINT piatto_del_giorno_piatto_fkey FOREIGN KEY (piatto) REFERENCES piatto(id);


--
-- Name: preparazione prepara_linea_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY preparazione
    ADD CONSTRAINT prepara_linea_fkey FOREIGN KEY (linea, giorno) REFERENCES linea_preparazione(id, giorno);


--
-- Name: preparazione prepara_ordine_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY preparazione
    ADD CONSTRAINT prepara_ordine_fkey FOREIGN KEY (ordine) REFERENCES ordine(id) ON DELETE CASCADE;


--
-- Name: preparazione prepara_piatto_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY preparazione
    ADD CONSTRAINT prepara_piatto_fkey FOREIGN KEY (piatto, giorno) REFERENCES piatto_del_giorno(piatto, giorno);


--
-- PostgreSQL database dump complete
--

