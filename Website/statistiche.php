<?php 
include 'header.php';
include 'functions.php';
check_login();
include 'navbar.php';
	
$giorni= getGiorni();
?>
<title>Admin</title>
</head>

<body>
	<h1 style="text-align:center;">linee</h1>
	<div class="row">
		<div class="container">
			<div class="col-md-6 col-md-offset-3" >
		<?php 
			echo '<form class="form-horizontal" style="margin-top:75px" href='.$_SERVER['PHP_SELF'].' method="post" enctype="multipart/form-data">
					
								<div class="form-group">
						<label>Seleziona un giorno per visionare la sua statistica.</label>
						<select class="form-control" name="giorno">
							';
			foreach ($giorni as $giorno) {
				
							echo '<option value="'.$giorno['giorno'].'">'.$giorno['giorno'].'</option>';
					
			}
			echo '</select>';
			echo '</div>';
				echo '<button class="btn btn-primary btn-sm active" >Visiona statistche</button>';
				echo '</form>';
		?>
		<hr>
			</div>
			<div class="col-md-12" >
			<?php
			if(isset($_POST['giorno']))
				printStatistica($_POST['giorno']);
			?>
		</div>
		</div>

	</div>
</body>
