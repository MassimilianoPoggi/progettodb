<?php 
	session_start();
	include 'header.php';
	include 'functions.php';
	include 'functions_external.php';
	include_once 'navbar.php';
	escapeNormalUser();

	$areeDiSpedizione=getAreeSpedizione();
	
	if(isset($_POST['raggio'])&&isset($_POST['indirizzo'])&&isset($_POST['tempo'])){
		$coord=get_coordinates($_POST['indirizzo'],"","");
		if($coord){
			insertAreeSpedizione($coord['lat'],$coord['long'],$_POST['raggio'],$_POST['tempo'],$_POST['indirizzo']);
		}
		else{
			//enable non settata
		}
	}

?>

    <title>Gestisci indirizzi</title>
   
	
  </head>
  <body>
 
		<div class="container">
		<h1 style="text-align:left;">Aggiungi un area di spedizione.</h1>
		 <p>Qui gestisci le aree di spedizione, per creare un nuovo punto di spedizione inserisci il centro (indirizzo completo) e definisci un raggio d' azione.
			Puoi anche rimuovere gli indirizzi.</p>  
   

		<?php echo '<form action="'.$_SERVER['PHP_SELF'].'" class="form-horizontal" style="margin-top:75px" method="post" enctype="multipart/form-data">';
			?> 
			 
			  <div class="row">
				<div class="col-sm-8">       
					<div class="form-group">
						<label for="city" class="col-sm-2 control-label">Tempo max di spedizione</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="city" placeholder="2 [minuti]" name="tempo">
						</div>
					</div>       
					<div class="form-group">
						<label for="city" class="col-sm-2 control-label">Indirizzo</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="city" placeholder="via giacomo... milano 20015 italia" name="indirizzo">
						</div>
					</div>
					<div class="form-group">
						<label for="km" class="col-sm-2 control-label">Raggio [metri]</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="m" placeholder="2.0" name="raggio">
						</div>
					</div>					
					<button type="submit" name="Aggiungi linea" class="btn btn-primary btn-lg active " style="text-align:center;">Aggiungi</button>
				</div>
			</div>
		</form>
		</div>
		<div class="container">

		<?php
		
							
							if(count($areeDiSpedizione)>0)
								printAreaSpedizione($areeDiSpedizione);
							else 
								echo '<br><p>Nessun indirizzo di spedizione inserito.</p>';
						?>
		</div>
	

  </body>
</html>