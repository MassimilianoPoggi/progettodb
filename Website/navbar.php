<nav class="navbar navbar-default">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
<?php
	if($_SESSION['privilege'] == 5){ 
			echo '<a class="navbar-brand" href="gestisciindirizzi.php"><strong>FoodExpress&trade;</strong></a>';
			echo '<a class="btn btn-info navbar-btn" href="admin_piatti.php" role="button">DB piatti</a>';
			echo '<a class="btn btn-info navbar-btn" href="preparazione_giornata.php" role="button">Piatti del giorno</a>';
			echo '<a class="btn btn-info navbar-btn" href="lineaProduzione.php" role="button">Linee di produzione</a>';
			echo '<a class="btn btn-info navbar-btn" href="gestiscispedizioni.php" role="button">Gestisci le spedizioni</a>';
			echo '<a class="btn btn-info navbar-btn" href="gestisciindirizzi.php" role="button">Indirizzi di spedizione</a>';
			echo '<a class="btn btn-info navbar-btn" href="statistiche.php" role="button">Statistiche</a>';
		 }
		 else{
		 	echo '<a class="navbar-brand" href="home.php"><strong>FoodExpress&trade;</strong></a>';
		 	echo '<a class="btn btn-info navbar-btn" href="ordini.php" role="button">I tuoi ordini</a>';
		 	echo '<ul class="nav navbar-nav navbar-right"><li>';	
			echo '				<button class="btn btn-default navbar-btn" onclick="location.href=\'carrello.php\'">Carrello
								<span class="badge">'
									.count($_SESSION['cart']).'
								</span>
								<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true" style="margin-right:5px;">
								</span>
							</button>
					
			</li>
			</ul><!-- /.navbar-collapse -->';
		 }
		?>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		
	</div><!-- /.container-fluid -->
</nav>	

<div class="pull-right" style="margin-top:-20px">Benvenuto, 
	<?php 
		if($_SESSION['privilege']==5){
			echo ($_SESSION['login'] . '! (<a href="home.php?logOut=1">Log Out</a>)');
			echo '<br><a href="changepassword.php">Cambia password</a></div>';
		}
		else{
			echo ($_SESSION['login'] . '! (<a href="home.php?logOut=1">Log Out</a>)');
			echo '<br><a href="changepassword.php">Cambia password</a></div>';
		}
	?>