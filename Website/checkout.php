<?php 
	include 'header.php';
	include 'functions.php';
	include 'functions_external.php';
	check_login();
	include 'navbar.php'; 
	escapeSuperUser();



	if( isset($_POST['indirizzo'])&& isset($_SESSION['cart'])&& count($_SESSION['cart']) > 0){
		

		$coord=get_coordinates($_POST['indirizzo'],"","");
		if($coord){
			$areaGeograficaAppartenenza = getAreaGograficaAppartenenza($coord['lat'],$coord['long']);

			if($areaGeograficaAppartenenza){
				$areaGeograficaAppartenenza=$areaGeograficaAppartenenza['area_geografica_appartenenza'];

				
			}
			else{
				$ExceptionNoAreaGeoFound=$_POST['indirizzo'];
			 echo '<div class="alert alert-danger" role="alert">Nessun area geografica disponibile per questo indirizzo.</div>';
				unset($_POST['indirizzo']);
			}


		}
		else{
			echo '<div class="alert alert-danger" role="alert">Indirizzo non corretto</div>';
		}
		
	}

	if(isset($_POST['time'])){
		$idPiatti=parseIdPiatti($_SESSION['cart']);
		
		$idPiatti= to_pg_array($idPiatti);
		$result = aggiungi_ordine($_SESSION['login'],$_POST['indirizzo'],$areaGeograficaAppartenenza,$_POST['time'],$idPiatti);
		
		if($result){
			$ordineAggiunto=$result;
			$_SESSION['cart']=array();

		}
		else{
			echo '<div class="alert alert-danger" role="alert">Errore aggiungi ordine</div>' ;
		}
	}

?> 
<script>
function showForm(){
	(document.getElementById("formDatiFatturazione")).style.display = "block";
}
</script>
<title>Checkout</title>
</head>
<body>
	<?php
		if(count($_SESSION['cart'])==0 ){
			if(isset($ordineAggiunto))
				$faseFatturazione=true;
			else{
				header("location: carrello.php");
			}
		}
		else{
			printCarrello();
		}

	?>
	

	<div class="container">
			<?php
					if(!isset($_POST['indirizzo'])){
						echo '<div class="row col-sm-6">
								<form action="'. $_SERVER['REQUEST_URI'].'"  data-toggle="validator" role="form"  method="post" >
									<div class="form-group">
										<label class="col-sm-2 control-label">Ind. di spedizione: </label>
										<div class="col-sm-8">
											<input type="text" class="form-control" name="indirizzo" id="exampleInput1" required/>
										</div>
									  </div>
									<div class="form-group">
										<div class="col-sm-2">
											<button name="submit" class="btn btn-primary btn-lg active" onClick="showForm();">prosegui</button>
										</div>
										
									</div>
								</form>
							</div>
						';
					}
					else if(!isset($_POST['time'] ) &&isset($areaGeograficaAppartenenza) &&isset($_POST['indirizzo'])){
						echo '<div class="row">
							<form action="'. $_SERVER['REQUEST_URI'].'" id=""  data-toggle="validator" role="form" class="form-horizontal" method="post" >
								<div class="form-group has-feedback">
									<label class="col-sm-2 control-label">Ora di arrivo voluta:</label>
										<div class="col-sm-6">
											<input type="time" class="form-control" id="inputEmail3" name="time" required/>
											<input type="hidden" name="area" value="'.$areaGeograficaAppartenenza.'"/>
											<input type="hidden" name="indirizzo" value="'.$_POST['indirizzo'].'"/>
										</div>
								</div>
							<div class="form-group">
								<button name="submit" class="btn btn-primary btn-lg active" onClick="showForm();">prosegui</button>
							</div>
							</form>
						 </div>
						';
							}
					else if(isset($faseFatturazione)){
						echo '			<div class="container">
								<h3> Il tuo ordine è stato eseguito !!</h3>
								<br>
								Desideri la fattura ? ==>
								<a  class="btn btn-primary btn-lg active" onClick="showForm();">Fatturazione</a>
								<a  class="btn btn-primary btn-lg active"href="ordini.php">I tuoi ordini</a>
						</div>';

						echo '
					<form action="ordini.php" id=""  data-toggle="validator" role="form" class="form-horizontal" method="post" >
						<div id="formDatiFatturazione" style="display: none">
				
							<b class="col-sm-2 ">        Dati fatturazione: </b><br>
						
							<div class="form-group">
								<label class="col-sm-2 control-label">Codice fiscale</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="inputEmail3" name="piva">
								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label">Intestazione</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="inputEmail3" name="intestazione">
								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label">Indirizzo fiscale</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="inputEmail3" name="indirizzo_fiscale" >
								</div>
							</div>
							<input type="hidden" name="id_ordine" value="'.$ordineAggiunto.'">
							<div class="form-group">
								<button name="submit" class="btn btn-primary btn-lg active" >Aggiungi fatturazione</button>
							</div>
						</div>
						
					</form>
				';
					}
				?>
				

						
	</div>					

	
	
</body>