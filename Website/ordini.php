<?php 
	include 'header.php';
	include 'functions.php';
	 	check_login();
	include 'navbar.php'; 
	escapeSuperUser();

	if(isset($_POST['piva'])&&isset($_POST['intestazione'])&&isset($_POST['indirizzo_fiscale'])&&isset($_POST['id_ordine'])){
		insertFattura($_POST['id_ordine'],$_POST['piva'],$_POST['indirizzo_fiscale'],$_POST['intestazione']);
	}

	if(isset($_GET['rm'])){
		deleteOrdine($_GET['rm']);
	}
	$ordini = getOrdini($_SESSION['login']);
	
	
?>

<?php include 'header.php' ?>

<title>
	I tuoi ordini:
</title>
</head>
<body>
	<div class="container">
	<?php
		echo '<div class="row">
		<h2 class ="container-fluid">I tuoi ordini</h2>';
		echo '
		<ul class="list-group col-md-6">
	<div class="container-fluid">';
			echo '<p>Puoi visionare i tuoi ordini in attesa di completamento.</p>';
			if(count($ordini) == 0){ 
				echo '<div class="alert" role="alert">Storico ordini vuoto.</div>';
			 } else {
				printOrdiniSospesi($ordini,array("In attesa","In preparazione","Pronto","In consegna"));
			} 
	echo "</div>";	
		echo '</ul>';

	echo '
		<ul class="list-group col-md-6">
			<div class="container-fluid">';
			echo '<p>Ordini evasi.</p>';
			if(count($ordini) == 0){ 
				echo '<div class="alert" role="alert">Storico ordini vuoto.</div>';
			 } else {
				printOrdini($ordini,array("Consegnato"));
			} 
	echo "</div>";
		echo '</ul>';

		echo '</div>';
	?>

			<?php
		
		if(isset($_GET['info'])){
			echo '<div class="row">
		<h2 class ="col-md-6 col-md-offset-3">Il tuo ordine n° '.$_GET['info'].', contiene: </h2>
		<hr>';
			printPlate(getInfoOrdine($_GET['info']));
			echo '</div>';
		}
		
		?>

	</div>
</body>
</html>