<?php 
	include 'header.php';
	include 'functions.php';
	check_login();
	include 'navbar.php'; 
	escapeSuperUser();
	if(isset($_GET['changepassword'])){
		echo '<div class="alert alert-info" role="alert">Password cambiata.</div>';
	}

	if(isset($_POST['delete'])){
		elimina_piatto($_POST['id']);
	}

	if (isset($_POST['add'])) {
		$tp = array($_POST['id'], $_POST['nome'], $_POST['prezzo']);
		array_push($_SESSION['cart'], $tp);
	}

	$sql = "SELECT id,titolo,path_foto,descrizione,prezzo,quantita FROM piatto_del_giorno JOIN piatto ON piatto.id=piatto_del_giorno.piatto AND piatto_del_giorno.giorno=current_date  ";
	$result = pg_query($sql);
	
	$piattiDelGiorno=array();
	
		while ($row = pg_fetch_assoc($result)) { 
			if($row['quantita']>0)
				array_push($piattiDelGiorno,$row);
		}
			
?>

<title>Home</title>
</head>
<body>

	

	
	
	<div class="container">

		<?php
			if(count($piattiDelGiorno))
				echo "<h2>Componi il tuo ordine</h2>";
			else{
				echo "<h2>Contatta l' admin.<br>Piatti non disponibili.</h2>";
			}
	
		?>

	<?php 
		$c = 0;

		foreach ($piattiDelGiorno as $row) {
			if ($c % 3 == 0) {
				echo '<div class="row">';
			} 
			$c++; ?>
			<form method="post">
				<input value="<?php echo $row['id'] ?>" type="hidden" name="id"/>
				<input value="<?php echo $row['titolo'] ?>" type="hidden" name="nome"/>
				<input value="<?php echo $row['prezzo'] ?>" type="hidden" name="prezzo"/>
				<div class="col-md-4">
					<div class="thumbnail">
						<img src="<?php 
										if($row['path_foto']) 
											echo $row['path_foto']; 
										else 
											echo "/img/placeholder.jpg"; ?>" alt="">
						<div class="caption">
							<h3><?php echo $row['titolo'] ?> - <?php echo $row['prezzo'] ?>€</h3>
							<p><?php echo $row['descrizione'] ?></p>
							<p>
								<span class="label label-pill label-info">Disp.: <?php echo $row['quantita'];?></span>
								<button class="btn btn-primary" role="button" type="submit" name="add">Aggiungi al Carrello</button> 
							</p>
						</div>
					</div>
				</div>
			</form>
			<?php 
			if ($c % 3 == 0) {
				echo '</div>';
			}

		} ?>
	</div>
</body>
</html>