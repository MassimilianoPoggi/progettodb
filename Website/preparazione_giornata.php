<?php 
include 'header.php';
include 'functions.php';
check_login();
include 'navbar.php';
escapeNormalUser();
$paginazione=3;

if(isset($_GET['changepassword'])){
		echo '<div class="alert alert-info" role="alert">Password cambiata.</div>';
}

if(isset($_POST['submit'])&&isset($_POST['nome'])&&isset($_POST['categoria'])){
	if($_POST['nome']!=""&&$_POST['categoria']!=""){
		insertNuovaLinea($_POST['categoria'],$_POST['nome']);
	}
	else{
		echo ' <div class="alert alert-danger" role="alert">Campi vuoti !!</div>';
	}
}

if(isset($_GET['newday'])){
	iniziaNuovaGiornata();
}
if(isset($_POST['item'])){
	handleInsertPlateOfTheDay($_POST['item']);
}

if(isset($_GET['pg'])){
	$pageNumber=$_GET['pg'];
}
else{
	$pageNumber=0;
}

$plates = listAllPlate();

if(count($plates)-$pageNumber*$paginazione>$paginazione)
	$showNextPage=true;
else 
	$showNextPage=false;

?>
<title>Admin</title>
</head>

<body>

	<div class="container">
		<h1 style="text-align:center;">Gestisci la giornata</h1>
		<p>Qui puoi inserire nuovi piatti del giorno, iniziare una nuova giornata, creare delle linee di lavoro.</p>
	
		<?php
		echo '<a class="btn btn-primary btn-lg active" href="'.$_SERVER['PHP_SELF'].'?newday=1">INIZIA NUOVO GIORNO</a>';
		?>
<div class="row">
		<div class="col-md-6 ">
			<?php echo '<form action="'.$_SERVER["REQUEST_URI"].'" class="form-horizontal" style="margin-top:75px" method="post" enctype="multipart/form-data">';
			?> 

				<div class="container">
				  <?php 

				  if(count($plates)==0){
				  	echo '<p>Aggiungi piatti nel database: <a href="admin_piatti.php">pagina dei piatti</a></p>';
				  }
				  else{
					  echo '
					  <h2>Piatti</h2>    
					  <p>Seleziona ii piatti del giorno che vuoi aggiungere nel tuo menù del giorno</p>                  
					  <table class="table table-striped">
						<thead>
						  <tr>';
							
								foreach(array_keys($plates[0]) as $temp){
									echo "<th>".$temp. "</th>";
								}
							echo '
							<th>prezzo</th>
							<th>quantità</th>
						  </tr>
						</thead>
						<tbody>';
								printPlatePage($plates,$pageNumber,$paginazione);
						
						echo '</tbody>
					  </table>
					</div>
				<button type="submit" name="Aggiungi" class="btn btn-primary btn-lg active " style="text-align:center;">Aggiungi piatti</button>
			</form>';
		}
		?>
		
				
		<?php

			if(	$pageNumber!=0)
				echo '<span class="pull-right"><a class="btn btn-primary btn-lg active" href="'.$_SERVER['PHP_SELF']."?pg=".($pageNumber-1).'">Vai a pagina : '.($pageNumber-1).'</a></span>';
			if($showNextPage==true)
				echo '<span class="pull-right"><a class="btn btn-primary btn-lg active" href="'.$_SERVER['PHP_SELF']."?pg=".($pageNumber+1).'">Vai a pagina : '.($pageNumber+1).'</a></span>';
		?>
			
		</div>
<div class="col-md-6 col-md-offset-3">
			<form class="form-horizontal" style="margin-top:75px" method="post">
				<div class="form-group">
					<label for="lastName" class="col-sm-3 control-label">Nome</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="lastName" placeholder="Nome" name="nome">
					</div>
				</div>
				<div class="form-group">
						<label class="col-sm-3 control-label">Categoria</label>
				<div class="col-sm-6">
						<select class="form-control"  name="categoria">
							
							<?php 
									$arrayCategoria=getCategorie();
									 foreach($arrayCategoria as $row){
										 echo '<option value="'.$row['nome'].'">'.$row['nome'].'</option>';
									 }
								?>
						</select>	
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-10">
						<button type="submit" name="submit" class="btn btn-primary btn-lg active">Aggiungi linea</button>
					</div>
				</div>
			</form>
		</div>	
	</div>
</div>
</div>




</body>
