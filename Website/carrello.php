<?php 
	include 'header.php';
	include 'functions.php';
	check_login();
	escapeSuperUser();
	if(isset($_GET['rm'])){
		unset($_SESSION['cart'][$_GET['rm']]);
	}
?>

<title>Carrello</title>
</head>
<body>
	<?php include 'navbar.php'; ?>
	<div class="container">
				<h2>Carrello</h2>
		<ul class="list-group col-md-6">
			<?php 
			if(count($_SESSION['cart']) == 0){ ?>
				<div class="alert alert-info" role="alert">Il carrello è vuoto.</div>
			<?php }
			 else {
				echo '<table class="table table-striped"><thead><tr>';
	echo "<th>Nome</th>";
	echo "<th>Costo</th>";
	echo "<th> </th>";
	echo '</tr></thead>
			<tbody>';
	foreach($_SESSION['cart'] as $key => $value){
		echo '<tr>';
			echo '<td>'.$value[1].'</td>';
			echo '<td>'.$value[2].'</td>';
			echo '<td><a type="button" class="btn btn-danger" href="'.$_SERVER['PHP_SELF'].'?rm='.$key.'">
  <span class="glyphicon glyphicon-trash" aria-hidden="true" ></span>
</a></td>';
		echo '</tr>';
	}
	echo '</tbody>';
	echo '</table>';
	
	echo '<li class="list-group-item active">Totale: <p class="pull-right">'.totale().' €</p></li>';

			}
	?>		
		<div class="container">
		<?php
			if(count($_SESSION['cart']) > 0)
			echo '<a class="btn btn-info navbar-btn" href="checkout.php" role="button">Checkout</a>';
		?>

</div>
</div>
	
</body>