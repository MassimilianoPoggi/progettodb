<?php

include 'header.php';
include 'functions.php';
check_login();
include 'navbar.php';

	if(isset($_POST['password'])&&isset($_POST['passwordbis'])){
		if ($_POST['password'] != $_POST['passwordbis']) {
			echo '<div class="alert alert-danger" role="alert">Le password non coincidono.</div>';
		}
		elseif (strlen($_POST['password']) == 0 || strlen($_POST['passwordbis']) == 0) { 
			echo '<div class="alert alert-danger" role="alert">Password vuota non valida.</div>';
		
		}else{
			
			$db = open_pg_connection();
			if($_SESSION['privilege'] == 5)
				$sql = " SELECT cambia_password_ristoratore($1,$2) ";
			else
				$sql = " SELECT cambia_password_cliente($1,$2) ";

			$result = pg_prepare($db,"signupquery",$sql);
			$params = array($_SESSION['login'],hash('sha256',$_POST['password']));
			$result = pg_execute($db, "signupquery",$params);
			$result = pg_fetch_assoc($result);
			printWithPre($result);
			if ($result) {
				echo '<div class="alert alert-danger" role="alert">Password cambiata.</div>';
				if($_SESSION['privilege'] != 5)
					header("location: home.php?changepassword=1");
				else{
					header("location: preparazione_giornata.php?changepassword=1");
				}
			}
			else{
				echo '<div class="alert alert-danger" role="alert">errore insert new user.</div>';
			}
		}
	}
	else{

	}
	
?>


<title>
	Sign Up!
</title>
</head>
<body>
	<div class="container">
		<div class="pull-right" style="margin-top:25px">
			<a href="home.php">Home</a>
		</div>
		<div class="col-md-6 col-md-offset-3">
			<form class="form-horizontal" style="margin-top:75px" method="post">
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-3 control-label">Nuova password</label>
					<div class="col-sm-6">
						<input type="password" class="form-control" id="inputPassword3" placeholder="Password" name="password">
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3bis" class="col-sm-3 control-label">Ripeti nuova password</label>
					<div class="col-sm-6">
						<input type="password" class="form-control" id="inputPassword3bis" placeholder="Ripeti Password" name="passwordbis">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-10">
						<button type="submit" class="btn btn-default" name="signup">Cambia password</button>
					</div>
				</div>
			</div>
		</div>
	</form>
</body>
</html>