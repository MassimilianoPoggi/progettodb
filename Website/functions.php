<?php 
define ("myhost", "WEBAPP_DB_ADDRESS");
define ("myuser", "WEBAPP_USER");
define ("mypsw", "WEBAPP_USER_PASSWORD");
define ("mydb", "WEBAPP_DB_NAME");

function insertNuovaLinea($cat,$nome){
	$db = open_pg_connection();
	$sql='SELECT  crea_linea_preparazione($1,$2)';
	$result = pg_prepare($db,"insertNuovaLinea",$sql);
	$params = array($cat,$nome);
	$result = pg_execute($db,"insertNuovaLinea",$params);

	if($result){
		$fetched=pg_fetch_assoc($result);
		printWithPre($fetched);
		echo '<div class="alert alert-info" role="alert">Inserita nuova linea</div>';
	}
	else{
		echo '<div class="alert alert-danger" role="alert">Errore inserimento nuova linea</div>';
	}
}
function iniziaNuovaGiornata(){
	$db = open_pg_connection();
	$sql='SELECT  inizia_nuova_giornata()';
	$result = pg_prepare($db,"iniziaNuovaGiornata",$sql);
	$params = array();
	$result = pg_execute($db,"iniziaNuovaGiornata",$params);

	if($result){
		$fetched=pg_fetch_assoc($result);
		echo '<div class="alert alert-info" role="alert">Iniziata nuova giornata!!</div>';
	}
	else{
		echo '<div class="alert alert-danger" role="alert">Errore inserimento nuova linea</div>';
	}
}
function printStatistica($giorno){
	$db = open_pg_connection();
	$sql='SELECT  linea ,num_piatto,tempo_lavoro,clienti_serviti,prezzo_medio_piatto FROM statistiche_linee WHERE giorno=$1';
	$result = pg_prepare($db,"getStatistiche",$sql);
	$params = array($giorno);
	$result = pg_execute($db,"getStatistiche",$params);
	
	if($result){
		$array=array();
		while($row = pg_fetch_assoc($result)){
			array_push($array, $row);
		}
		
		echo '<table class="table "><thead><tr>';
		echo "<th>Id linea</th>";
		echo "<th>Numero piatti</th>";
		echo "<th>Tempo di lavoro</th>";
		echo "<th>Clienti serviti</th>";
		echo "<th>Prezzo medio piatto</th>";
		echo '</tr></thead>
				<tbody>';
		foreach($array as $row){
				echo '<tr>';
				echo "<td>".$row['linea']."</td>";
				echo "<td>".$row['num_piatto']."</td>";
				echo "<td>".$row['tempo_lavoro']."</td>";
				echo "<td>".$row['clienti_serviti']."</td>";
				echo "<td>".number_format($row['prezzo_medio_piatto'],2,'.','')."</td>";
			echo '</tr>';
	}
	echo '</tbody></table>';
	}
	else{
		echo '<div class="alert alert-info" role="alert">Errore aggiornamento.</div>';
	}
}
function getGiorni(){
	$db = open_pg_connection();
	$sql='SELECT DISTINCT giorno FROM statistiche_linee';
	$result = pg_query($sql);
	if($result){
		$array=array();
		while($row = pg_fetch_assoc($result)){
			array_push($array, $row);
		}
		return $array;
	}
	else{
		false;
	}
}

function aggiornaStatoOrdine($id,$nextState){
	$db = open_pg_connection();
	$sql='SELECT aggiorna_stato_ordine($1,$2)';
	$result = pg_prepare($db,"updateStateOrdine",$sql);
	$params = array($id,$nextState);
	$result = pg_execute($db,"updateStateOrdine",$params);
	if($result){
		echo '<div class="alert alert-success" role="alert">Aggiornato con successo</div>';
	}
	else{
		echo '<div class="alert alert-info" role="alert">Errore aggiornamento.</div>';
	}
}
function printPlate($array){
	echo '<table class="table table-striped"><thead><tr>';
	echo '<th>Nome</th>';
	echo '<th>Descrizione</th>';
	echo '<th>Categoria</th>';
	foreach($array as $plate){
			echo "<tr>";
				echo "<td>".$plate['titolo']."</td>";
				echo "<td>".$plate['descrizione']."</td>";
				//echo "<td>".$plate['tempo']."</td>";
				//echo "<td>".$plate['complessita']."</td>";
				echo "<td>".$plate['categoria']."</td>";
	echo "</tr>";
	}
	echo "</tbody></table>";
}
function getInfoOrdine($id){
	$db = open_pg_connection();
	$sql='SELECT * FROM piatto JOIN (SELECT piatto FROM preparazione WHERE ordine = $1) as p ON p.piatto = piatto.id';
	$result = pg_prepare($db,"getInfoOrdine",$sql);
	$params = array($id);
	$result = pg_execute($db,"getInfoOrdine",$params);
	if($result){
		$array=array();
		while($row = pg_fetch_assoc($result)){
			array_push($array, $row);
		}
		return $array;
	}
	else{
		echo '<div class="alert alert-info" role="alert">Errore nessun piatto.</div>';
	}
	
}
function printPreparazioniStates($id){
	$db = open_pg_connection();
	$sql='SELECT piatto.titolo AS titolo, preparazione.stato AS stato, linea_preparazione.nome AS linea
			FROM piatto JOIN preparazione ON piatto.id = preparazione.piatto
			JOIN linea_preparazione ON preparazione.linea = linea_preparazione.id AND preparazione.giorno = linea_preparazione.giorno
			WHERE preparazione.ordine = $1';
	$result = pg_prepare($db,"printPreparazioniStates",$sql);
	$params = array($id);
	$result = pg_execute($db,"printPreparazioniStates",$params);
	if($result){
		echo '<table class="table table-striped"><thead><tr>';
		echo '<th>Piatto</th>';
		echo '<th>Stato preparazione</th>';
		echo '<th>Linea preparazione</th>';
		while($row = pg_fetch_assoc($result)){
			echo "<tr>";
				echo "<td>".$row['titolo']."</td>";
				echo "<td>".$row['stato']."</td>";
				echo "<td>".$row['linea']."</td>";
			echo "</tr>";
		}
	}
	else {
		echo '<div class="alert alert-info" role="alert">Errore nessun piatto.</div>';
	}
}
function fromOrdineToNome($id){
	$db = open_pg_connection();
	$sql='SELECT nome, cognome FROM cliente JOIN ordine ON cliente.login = ordine.cliente WHERE ordine.id = $1';
	$result = pg_prepare($db,"fromOrdineToNome".$id,$sql);
	$params = array($id);
	$result = pg_execute($db,"fromOrdineToNome".$id,$params);
	if($result){
		$fetched=pg_fetch_row($result);
		return $fetched[0] ." " .$fetched[1] ;

	}
	else{
		return "ERRORE";
	}
}
function getTotale($ordine){
	$db = open_pg_connection();
	$sql='SELECT SUM(prezzo) FROM piatto_del_giorno JOIN preparazione ON piatto_del_giorno.piatto = preparazione.piatto AND piatto_del_giorno.giorno = preparazione.giorno WHERE preparazione.ordine = $1';
	$result = pg_prepare($db,"getTotale".$ordine,$sql);
	$params = array($ordine);
	$result = pg_execute($db,"getTotale".$ordine,$params);
	if($result){
		$fetched=pg_fetch_row($result);
		return $fetched[0];
	}
	else{
		return null;
	}
}
function printFattura($id){
	$db = open_pg_connection();
	$sql='SELECT * FROM fattura   WHERE ordine=$1';
	$result = pg_prepare($db,"getFattura",$sql);
	$params = array($id);
	$result = pg_execute($db,"getFattura",$params);
	if($result){
		$fetched=pg_fetch_assoc($result);
		echo '<table class="table table-striped"><thead>
		<tr>';
		echo "<th>Importo totale</th>";
		echo "<th>Partita iva</th>";
		echo "<th>Indirizzo fiscale</th>";
		echo "<th>Nome società</th>";
		echo '
		</tr>
		</thead>
		<tbody><tr>';
		echo "<th>".getTotale($id)."</th>";
		echo "<th>".$fetched['piva_cfisc']."</th>";
		echo "<th>".$fetched['indirizzo_fiscale']."</th>";
		if(isset($fetched['nome_societa'])&& $fetched['nome_societa']!="")
			echo "<th>".$fetched['nome_societa']."</th>";
		else
			echo "<th>".fromOrdineToNome($id)."</th>";
		echo '</tr>
		</tbody>
		</table>';
	}
	else{
		echo '<div class="alert alert-info" role="alert">Errore nessuna fattura.</div>';	
	}
}
function printOrdiniRistoratore($arr,$arrayState){
	echo '<table class="table table-striped"><thead><tr>';
		
		if(count($arr)>0){
		
			echo "<th>Id </th>";
		echo "<th>Indirizzo </th>";
		echo "<th>Ora consegna </th>";
		echo "<th>Stato </th>";
		echo '<th></th><th>Fattura</th>
		</tr></thead><tbody>';

		foreach ($arr as $elem ) {
			if(in_array($elem['stato'],$arrayState)){
				echo '<tr>';
				//stampo gli elementi di cui so i "valori" TODO ERASE
					echo '<td><a type="button" class="btn btn-info" href="'.$_SERVER['PHP_SELF'].'?info='.$elem['id'].'">
							  <span class="glyphicon" aria-hidden="true" >'. $elem['id'].'</span>
							</a></td>';
					echo "<td>".$elem['indirizzo_spedizione']."</td>";
					echo "<td>".$elem['ora_consegna']."</td>";
					echo "<td>".$elem['stato']."</td>";
				$nextState="";
				switch($elem['stato']){
					case "In attesa": $nextState='In preparazione';
											$color='btn-danger';
											$disabled=true;
							break;
					case "In preparazione": $nextState='Pronto';
											$color='btn-warning';
											$disabled=true;
							break;
					case "Pronto": $nextState='In consegna';
											$color='btn-info';
											$disabled=false;
							break;
					case "In consegna": $nextState='Consegnato';
											$color='btn-success';
											$disabled=false;
							break;
					default: 
							$nextState='Consegnato';
							$color='btn-success';
							//non settato
				}
				if(isset($disabled)){
					if(! $disabled)
						echo '<td><a type="button" class="btn '.$color.'" href="'.$_SERVER['PHP_SELF'].'?update='.$nextState.'&id='.$elem['id'].'">
							  <span class="glyphicon glyphicon-ok" aria-hidden="true" ></span>
							</a></td>';
					else
						echo '<fieldset disabled><td><a type="button" class="btn '.$color.'" >
							  <span class="glyphicon glyphicon-ok" aria-hidden="true" ></span>
							</a></td></fieldset>';
				}
				else{
					echo '<td>Completato</td>';
				}	
				if(isset($elem['fattura']))			
					echo '<td><a type="button" class="btn btn-info" href="'.$_SERVER['PHP_SELF'].'?fattura='.$elem['id'].'">
							  <span class="glyphicon" aria-hidden="true" >Visiona fattura</span>
							</a></td>';
				else{
					echo '<td></td>';
				}

				echo '</tr>';
			}
		}
		echo '</tbody></table>';
	}
	else{
		echo 'Linea vuota';
	}
}
function getTuttiOrdini(){
	$db = open_pg_connection();
	$sql='SELECT id,indirizzo_spedizione ,ora_consegna ,stato , fattura.ordine as fattura FROM ordine LEFT JOIN fattura ON ordine.id = fattura.ordine';
	$result = pg_prepare($db,"getOrdine",$sql);
	$params = array();
	$result = pg_execute($db,"getOrdine",$params);

	if($result){
		$array=array();
		while($row = pg_fetch_assoc($result)){
			array_push($array, $row);
		}
		return $array;
	}
	else{
		echo '<div class="alert alert-info" role="alert">Errore inserimento fattura, contattarre l\' admin.</div>';
	}
}
function aggiornaStatoPreparazione($id,$nextState){
	$db = open_pg_connection();
	$sql='SELECT aggiorna_stato_preparazione($1,$2)';
	$result = pg_prepare($db,"updateStatePreparazione",$sql);
	$params = array($id,$nextState);
	$result = pg_execute($db,"updateStatePreparazione",$params);
	if($result){
		echo '<div class="alert alert-success" role="alert">Aggiornato con successo</div>';
	}
	else{
		echo '<div class="alert alert-info" role="alert">Errore aggiornamento.</div>';
	}
}
function deleteOrdine($id){
	$db = open_pg_connection();
	$sql='SELECT annulla_ordine($1)';
	$result = pg_prepare($db,"delOrdine",$sql);
	$params = array($id);
	$result = pg_execute($db,"delOrdine",$params);
	if($result){
		echo '<div class="alert alert-success" role="alert">Ordine num:'.$id.', annullato con successo !!</div>';
	}
	else{
		echo '<div class="alert alert-info" role="alert">Errore inserimento fattura, contattarre l\' admin.</div>';
	}
}
function printOrdini($arr,$arrayState){
	echo '<table class="table "><thead><tr>';
	echo "<th>Id</th>";
	echo "<th>Indirizzo</th>";
	echo "<th>Ora di consegna prevista</th>";
	echo "<th>Stato</th>";
	echo "<th> </th>";
	echo '</tr></thead>
			<tbody>';
	foreach($arr as $row){
		if(in_array($row['stato'],$arrayState)){
			echo '<tr>';
			echo '<td><a type="button" class="btn btn-info" href="'.$_SERVER['PHP_SELF'].'?info='.$row['id'].'">
							  <span class="glyphicon" aria-hidden="true" >'. $row['id'].'</span>
							</a></td>';
			echo "<td>".$row['indirizzo_spedizione']."</td>";
			if($row['stato']!="Consegnato")
				echo "<td>".$row['ora_consegna']."</td>";
			echo "<td>".$row['stato']."</td>";
			if($row['stato']=="In attesa")
					echo '<td><a type="button" class="btn btn-danger" href="'.$_SERVER['PHP_SELF'].'?rm='.$row['id'].'">
	  <span class="glyphicon glyphicon-trash" aria-hidden="true" ></span>
	</a></td>';
			echo '</tr>';
		}
	}
	echo '</tbody></table>';
}

function printOrdiniSospesi($arr,$arrayState){
	echo '<table class="table "><thead><tr>';
	echo "<th>Id</th>";
	echo "<th>Indirizzo</th>";
	echo "<th>Ora di consegna prevista</th>";
	echo "<th>Stato</th>";
	echo "<th> </th>";
	echo '</tr></thead>
			<tbody>';
	foreach($arr as $row){
		if(in_array($row['stato'],$arrayState)){
			$db = open_pg_connection();
			$sql='SELECT stima_consegna_ordine($1)';
			$result = pg_prepare($db,"printOrdiniSospesi",$sql);
			$params = array($row['id']);
			$result = pg_execute($db,"printOrdiniSospesi",$params);
			$ora_consegna = explode(':', pg_fetch_assoc($result)['stima_consegna_ordine']);
			echo '<tr>';
			echo '<td><a type="button" class="btn btn-info" href="'.$_SERVER['PHP_SELF'].'?info='.$row['id'].'">
							  <span class="glyphicon" aria-hidden="true" >'. $row['id'].'</span>
							</a></td>';
			echo "<td>".$row['indirizzo_spedizione']."</td>";
			if($row['stato']!="Consegnato")
				echo "<td>".$ora_consegna[0].":".$ora_consegna[1]."</td>";
				echo "<td>".$row['stato']."</td>";
			if($row['stato']=="In attesa")
					echo '<td><a type="button" class="btn btn-danger" href="'.$_SERVER['PHP_SELF'].'?rm='.$row['id'].'">
	  <span class="glyphicon glyphicon-trash" aria-hidden="true" ></span>
	</a></td>';
			echo '</tr>';
		}
	}
	echo '</tbody></table>';
}

function getOrdini($login){
	$db = open_pg_connection();
	$sql='SELECT id,indirizzo_spedizione ,ora_consegna ,stato FROM ordine WHERE cliente = $1';
	$result = pg_prepare($db,"getOrdine",$sql);
	$params = array($login);
	$result = pg_execute($db,"getOrdine",$params);

	if($result){
		$array=array();
		while($row = pg_fetch_assoc($result)){
			array_push($array, $row);
		}
		return $array;
	}
	else{
		echo '<div class="alert alert-info" role="alert">Errore inserimento fattura, contattarre l\' admin.</div>';
	}
}
function insertFattura($ord,$piva,$indirizzo,$intestazione){
	$db = open_pg_connection();
	$sql='SELECT crea_fattura($1,$2,$3,$4)';
	$result = pg_prepare($db,"aggiungiOrdine",$sql);
	$params = array($ord,$piva,$indirizzo,$intestazione);
	$result = pg_execute($db,"aggiungiOrdine",$params);
	if($result){
		echo '<div class="alert alert-success" role="alert">Fattura registrata, provvederemo a spedirgliela con l\' ordine.</div>';
		
	}
	else{
		echo '<div class="alert alert-info" role="alert">Nessun ordine eseguito</div>';
	}
}
function printCarrello(){
	echo '
	<div class="container">
		<h2>Checkout</h2>
		<p>Stai confermando questo ordine</p>
		<ul class="list-group col-md-6">';
			
			if(count($_SESSION['cart']) == 0){ 
				echo '<div class="alert alert-info" role="alert">Il carrello è vuoto.</div>';
			 } else {
				foreach ($_SESSION['cart'] as $key => $value) { 
				echo '	<li class="list-group-item">'. $value[1].' <p class="pull-right">'.$value[2].' €</p></li>';
			 }
			} 
			echo '<li class="list-group-item active">Totale: <p class="pull-right">'.totale().' €</p></li>';

		
	echo "</div>";	
}
function getOraConsegna($login,$indirizzo,$areaGeograficaAppartenenza,$time,$idPiatti){
	$db = open_pg_connection();
	$sql='SELECT id FROM ordine WHERE cliente=$1 AND indirizzo_spedizione =  $2 AND ora_consegna = $3 AND area_geografica = $4';
	$result = pg_prepare($db,"aggiungiOrdine",$sql);
	$params = array($login,$indirizzo,$time,$areaGeograficaAppartenenza);
	$result = pg_execute($db,"aggiungiOrdine",$params);
	if($result){
		return ($result);
	}
	else{
		return false;
	}
}
function aggiungi_ordine($login,$indirizzo,$areaGeograficaAppartenenza,$time,$idPiatti){
	$db = open_pg_connection();
	$sql='SELECT crea_ordine ($1, $2,$3,$4,$5)';
	$result = pg_prepare($db,"aggiungiOrdine",$sql);
	$params = array($login,$indirizzo,$areaGeograficaAppartenenza,$time,$idPiatti);
	$result = pg_execute($db,"aggiungiOrdine",$params);
	if($result){
		return pg_fetch_assoc($result)['crea_ordine'];
	}
	else{
		echo '<div class="alert alert-danger" role="alert">Errore inserimento ordine</div>';
		return false;
	}
}
function getAreaGograficaAppartenenza($lat,$long){
	$db = open_pg_connection();
	$sql='SELECT area_geografica_appartenenza ($1, $2)';
	$result = pg_prepare($db,"getArea",$sql);
	$params = array($lat,$long);
	$result = pg_execute($db,"getArea",$params);
	if($result){
		return pg_fetch_assoc($result);
	}
	else{
		return false;
	}
}

function parseIdPiatti($array){
	$arr=array();
	foreach ($array as $elem) {
		array_push($arr, $elem[0]);
	}
	return $arr;
}
function to_pg_array($set) {
    settype($set, 'array'); // can be called with a scalar or array
    $result = array();
    foreach ($set as $t) {
        if (is_array($t)) {
            $result[] = to_pg_array($t);
        } else {
            $t = str_replace('"', '\\"', $t); // escape double quote
            if (! is_numeric($t)) // quote only non-numeric values
                $t = '"' . $t . '"';
            $result[] = $t;
        }
    }
    return '{' . implode(",", $result) . '}'; // format
}

function getPiattiLinea($id){
	$db = open_pg_connection();
	$sql='SELECT id,tempo_consegna,raggio_metri,nome,latitude(centro),longitude(centro) FROM area_geografica';
	$result = pg_prepare($db,"getArea",$sql);
	$params = array();
	$result = pg_execute($db,"getArea",$params);
	$array=array();
	while($row = pg_fetch_assoc($result)){
		array_push($array, $row);
	}
	return $array;
}


function insertAreeSpedizione($lat,$long,$raggio,$tempo,$descr){
	$db = open_pg_connection();
	$sql='SELECT crea_area_geografica($1,$2,$3,$4,$5)';
	$result = pg_prepare($db,"insertarea",$sql);
	$params = array($descr,$lat,$long,$raggio,$tempo);
	$result=pg_execute($db,"insertarea",$params);
	if($result){
			echo '<div class="alert alert-success" role="alert">Registrazione di '.$descr.' avvenuta con successo!</div>';
		}
	else{
			echo '<div class="alert alert-danger" role="alert">errore pg_execute</div>';
	}
}
function getAreeSpedizione(){
	$db = open_pg_connection();
	$sql='SELECT id,nome as Indirizzo,raggio_metri as Raggio,tempo_consegna as Tempo ,latitude(centro),longitude(centro) FROM area_geografica';
	$result = pg_prepare($db,"getArea",$sql);
	$params = array();
	$result = pg_execute($db,"getArea",$params);
	$array=array();
	while($row = pg_fetch_assoc($result)){
		array_push($array, $row);
	}
	return $array;
}

function printAreaSpedizione($arr){
	echo '<table class="table table-striped"><thead><tr>';
	echo "<th>Id</th>";
	echo "<th>Indirizzo</th>";
	echo "<th>Raggio</th>";
	echo "<th>Tempo di consegna</th>";
	echo "<th>Latitudine</th>";
	echo "<th>Longitudine</th>";
	echo '</tr></thead>
			<tbody>';
	foreach($arr as $row){
		echo '<tr>';
		foreach($row as $elem)
			echo "<td>$elem</td>";
		echo '</tr>';
	}
	echo '</tbody></table>';
}


function getCategorie(){
	$db = open_pg_connection();
	$sql='SELECT * FROM categoria';
	$result = pg_prepare($db,"cat",$sql);
	$params = array();
	$result = pg_execute($db,"cat",$params);
	if($result){
		$array=array();
		while($row = pg_fetch_assoc($result)){
			array_push($array, $row);
		}
		return $array;
	}
	else{
		echo '<div class="alert alert-danger" role="alert">Fallita getCategorie.</div>';
	}
}



function insertNewAddress($city, $street, $province,$id){
	$db = open_pg_connection();
	$sql='INSERT INTO indirizzo (via,città,provincia,id_u)  VALUES ($1, $2, $3,$4)';
	$result = pg_prepare($db,"insertAddress",$sql);
	$params =array($city, $street, $province,$id);
	$result = pg_execute($db,"insertAddress",$params);
	if($result){
		echo "insert success";
	}
	else
		echo "error on insert";
}
function printLinea($arr){
		echo '<table class="table table-striped"><thead><tr>';
	foreach(array_keys($arr[0]) as $elem)
		echo "<th>$elem</th>";
		echo '</tr></thead><tbody>';
		foreach($arr as $row){
			echo '<tr>';
			foreach($row as $elem)
				echo "<th>$elem</th>";
			echo '</tr>';
		}
	echo '</tbody></table>';
	
}

/*
data una preparazione linea stampa : per ogni preparazione: preparazinoe/piatto/ordine/stato
*/
function printPreparazioneLinea($arr){
echo '<table class="table table-striped"><thead><tr>';
		if(count($arr)>0){
		foreach(array_keys($arr[0]) as $elem){
			echo "<th>$elem</th>";
		}
		echo '<th> </th></tr></thead><tbody>';

		foreach ($arr as $elem ) {
			echo '<tr>';
			//stampo gli elementi di cui so i "valori" TODO ERASE
			foreach ($elem as $field ) {
				echo "<td>$field</td>";
			}

			$nextState="";
			switch($elem['stato']){
				case "In attesa": $nextState='In preparazione';
										$color='btn-danger';
						break;
				case "In preparazione": $nextState='Completato';
										$color='btn-warning';
						break;
				default: 
						$nextState='Completato';
						$color='btn-success';
			}

				echo '<td><a type="button" class="btn '.$color.'" href="'.$_SERVER['PHP_SELF'].'?update='.$nextState.'&id='.$elem['preparazione'].'">
		  <span class="glyphicon glyphicon-ok" aria-hidden="true" ></span>
		</a></td>';
			echo '</tr>';
		}
		echo '</tbody></table>';
	}
	else{
		echo 'Linea vuota';
	}
}

function getPreparazioneLinea($nome){
	$db = open_pg_connection();
	//impossibile eseguire pg prepare in quanto un arg. è nel join
	$sql = 'select preparazione.id AS preparazione, preparazione.ordine, piatto.titolo AS piatto, linea_preparazione.nome AS linea, preparazione.stato FROM preparazione JOIN "'.$nome.'" ON preparazione.id = "'.$nome.'".preparazione JOIN piatto ON preparazione.piatto = piatto.id JOIN linea_preparazione ON preparazione.linea = linea_preparazione.id AND preparazione.giorno = linea_preparazione.giorno';
	$result = pg_query($db, $sql);
	if($result){
		$array=array();
		while($row = pg_fetch_assoc($result)){
			array_push($array, $row);
		}
	return $array;
	}
	else{
		false;
	}
}
function getLineePreparazione(){
	$db = open_pg_connection();
	$sql='SELECT matviewname FROM pg_matviews WHERE schemaname=\'public\'';
	$result = pg_prepare($db,"getLinee",$sql);
	$params = array();
	$result = pg_execute($db,"getLinee",$params);
	if($result){
		$array=array();
		while($row = pg_fetch_row($result)){
			array_push($array, $row);
		}
		return $array;
	}
	else{
		false;
	}
}


function addDatiFatturazione($id,$nome,$addr,$cf){
	$db = open_pg_connection();
	$sql='INSERT INTO dati_fatturazione(id_utente,cf,nome,indirizzo_fiscale) VALUES ($1,$2,$3,$4)';
	$result = pg_prepare($db,"insertDatiFatt",$sql);
	$params = array($id,$cf,$nome,$addr);
	$result = pg_execute($db,"insertDatiFatt",$params);
}



function predicateCheck($row){
	return (isset($row["check"]))?true:false;
}
function handleInsertPlateOfTheDay($arr){
	$arr=array_filter($arr,"predicateCheck");
	$db = open_pg_connection();
	$sql = "INSERT INTO piatto_del_giorno (piatto,prezzo,quantita) VALUES ($1,$2,$3)";
		foreach($arr as $row){
				if(isset($row['check'])&& isset($row['prezzo']) && isset($row['quantità']) && $row['prezzo']!="" && $row['quantità']!=""){
					$str1='loginquery'.$row['id'];
					$result = pg_prepare($db,$str1,$sql);
					$params = array($row['id'],$row['prezzo'],$row['quantità'],);
					$result = pg_execute($db,$str1,$params);
					if($result){
						
					}
					else{
						echo '<div class="alert alert-danger" role="alert">Errore inserimento piatto del giorno. (può essere già stato inserito) </div>';
					}
				}
				else{
					echo '<div class="alert alert-danger" role="alert">Devi inserire anche prezzo e quantità.</div>';
				}
			
		}
	
}

function printPlatePage($array,$page,$paginazione){
	$i=0;
	foreach($array as $plate){
		if($i<($page + 1)*$paginazione && ($i>=($page*$paginazione))){
			echo "<tr>";
				echo'<td><input type="checkbox" name="item['.$plate['id'].'][check]"/></td>';
				echo "<td>".$plate['titolo']."</td>";
				echo "<td>".$plate['descrizione']."</td>";
				echo "<td>".$plate['tempo']."</td>";
				echo "<td>".$plate['complessita']."</td>";
				echo "<td>".$plate['categoria']."</td>";
				echo'<td><input type="value" name="item['.$plate['id'].'][prezzo]" placeholder="Prezzo"/></td>';
				echo'<td><input type="value"  name="item['.$plate['id'].'][quantità]" placeholder="Quantità"/></td>';
				echo'<input type="hidden" name="item['.$plate['id'].'][id]" value="'.$plate['id'].'"/>';
	echo "</tr>";
		}
		$i=$i+1;
	}
}
function listAllPlate(){
	$sql = "SELECT id,titolo,descrizione,tempo,complessita,categoria FROM piatto ";
	$db = open_pg_connection();
	$result = pg_prepare($db, "show",$sql);
	$result = pg_execute($db, "show",array());
	
	if ($result) {
			$array=array();
				while($row = pg_fetch_assoc($result)){
					array_push($array, $row);
				}
			return $array;
		}else{ 
			echo '<div class="alert alert-danger" role="alert">Failed query.</div>';
		}  
}
function escapeSuperUser(){
	if( $_SESSION['privilege'] == 5){
		header("location: 404.php");
	}
}
function escapeNormalUser(){
	if( $_SESSION['privilege'] != 5){
		header("location: 404.php");
	}
}

function printWithPre($arr){
	echo '<pre>';
	print_r($arr);
	echo '</pre>';
}
function check_login(){
	open_pg_connection();
	if(isset($_GET['logOut'])){
		unset($_SESSION);
		header("location: index.php");
	}
	else{
		session_start();

		if(!isset($_SESSION['login'])){
			//header("location: index.php");
			echo "<h1>NON SEI PIU' loggato !!!!</h1>";
		}
	}
}

function open_pg_connection() {

	$connection = "host=".myhost." dbname=".mydb." user=".myuser." password=".mypsw;
	return pg_connect ($connection);

} 

function login($login, $psw,$livello){
		$db = open_pg_connection();

		if($livello == 5)
			$sql = "SELECT autentica_ristoratore($1,$2)";
		else
			$sql = "SELECT autentica_cliente($1,$2)";

		$result = pg_prepare($db,"loginAdmin",$sql);
		$psw = $psw;
		$params = array($login,$psw);
		$result = pg_execute($db,"loginAdmin",$params);

		if($result){
			$valore=pg_fetch_row($result);
			if($livello == 5){
				if($valore[0]=="t"){
					session_start();
					$_SESSION['login'] = $login;
					$_SESSION['privilege'] = 5;
					$_SESSION['cart'] = array();// per testing TODO da eliminare
					header("location: gestisciindirizzi.php");
					return true;
				}
				else{
					echo '<div class="alert alert-danger" role="alert">Login errato admin.</div>';
					return false;
				}
			}
			else{
				if($valore[0]==$login){
				$result= pg_query("SELECT * FROM cliente WHERE cliente.login = '".$login."'");
					$fetched= pg_fetch_assoc($result);
					session_start();
					$_SESSION['login'] = $fetched['login'];
					$_SESSION['nome'] = $fetched['nome'];
					$_SESSION['cognome'] = $fetched['cognome'];
					$_SESSION['telefono	'] = $fetched['telefono'];
					$_SESSION['privilege'] = 0;
					$_SESSION['cart'] = array();
					header("location: home.php");
					return true;
				}
				else{
					echo '<div class="alert alert-danger" role="alert">Login errato cliente.</div>';
				}
			}
		}
		else{
			echo '<div class="alert alert-danger" role="alert">Utente o password invalidi.</div>';
		}
}

function creaPiatto($titolo,$path_foto,$desc,$tempo,$compl,$cat){
	$db = open_pg_connection();
		$sql = "SELECT crea_piatto ($1, $2, $3, $4, $5,$6);";
		$result = pg_prepare($db, "submitquery",$sql);
		$params = array($titolo,$desc,$path_foto,$compl,$tempo,$cat);
		$result = pg_execute($db, "submitquery",$params);
		
		if ($result) {
			echo' <div class="alert alert-success" role="alert"><?php echo $_POST["nome"] ?> aggiunto/a con successo!</div> ';
		} else { 
			echo ' <div class="alert alert-danger" role="alert">Hai sbagliato qualcosa nell\' exec! Riprova.</div>';
		}

}

function creaCategoria($cat){
	$db = open_pg_connection();
		$sql = "SELECT crea_categoria($1)";
		$result = pg_prepare($db, "creaCategoria",$sql);
		$params = array($cat);
		$result = pg_execute($db, "creaCategoria",$params);
		if ($result) {
			echo' <div class="alert alert-success" role="alert">Categoria aggiunta con successo.</div> ';
		} else { 
			echo ' <div class="alert alert-danger" role="alert">Errore, categoria già presente</div>';
		}
}
function totale(){
	$tot = 0;
	foreach ($_SESSION['cart'] as $key => $value) {
		$tot = $tot + $value[2];
	}
	return $tot;
}

?>