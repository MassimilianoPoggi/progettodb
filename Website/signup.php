<?php 
	include 'header.php';
?>
<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
			</button>
			<a class="navbar-brand" href="index.php"><strong>FoodExpress&trade;</strong></a>
		</div>
	</div>
</nav>

<?php
	include "functions.php";
	if(isset($_POST['signup'])){
		if ($_POST['password'] != $_POST['passwordbis']) {
			echo '<div class="alert alert-danger" role="alert">Le password non coincidono.</div>';
		}
		elseif (strlen($_POST['nome']) == 0 && strlen($_POST['cognome']) == 0) { 
			echo '<div class="alert alert-danger" role="alert">Inserisci un nome e un cognome.</div>';
		
		}else{
			
			$db = open_pg_connection();
			$sql = " INSERT INTO cliente VALUES ($1, $2, $3, $4, $5)";
			
			$result = pg_prepare($db,"signupquery",$sql);
			$params = array($_POST['login'],hash('sha256',$_POST['password']), $_POST['nome'], $_POST['cognome'], $_POST['telefono'] );
			$result = pg_execute($db, "signupquery",$params);
			

			if ($result) {
			
				header('location: index.php?success=1');
			}
			else{
				echo '<div class="alert alert-danger" role="alert">errore insert new user.</div>';
			}
		}
	}
	
?>

<?php include 'header.php' ?>

<title>
	Sign Up!
</title>
</head>
<body>
	<div class="container">
		<div class="pull-right" style="margin-top:25px">
			<a href="index.php">Login</a>
		</div>
		<div class="col-md-6 col-md-offset-3">
			<form class="form-horizontal" style="margin-top:75px" method="post">
				<div class="form-group">
					<label for="Username" class="col-sm-3 control-label">Username</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="Username" placeholder="Username" name="login">
					</div>
				</div>
				<div class="form-group">
					<label for="lastName" class="col-sm-3 control-label">Nome</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="lastName" placeholder="Nome" name="nome">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-3 control-label">Cognome</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="inputEmail3" placeholder="Cognome" name="cognome">
					</div>
				</div>
				<div class="form-group">
					<label for="inputNumber" class="col-sm-3 control-label">Numero di telefono</label>
					<div class="col-sm-6">
						<input type='tel'  title='Phone Number (Format: +99(99)9999-9999)' class="form-control" id="inputNumber" placeholder="033-123131213" name="telefono">
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-3 control-label">Password</label>
					<div class="col-sm-6">
						<input type="password" class="form-control" id="inputPassword3" placeholder="Password" name="password">
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3bis" class="col-sm-3 control-label">Ripeti Password</label>
					<div class="col-sm-6">
						<input type="password" class="form-control" id="inputPassword3bis" placeholder="Ripeti Password" name="passwordbis">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-10">
						<button type="submit" class="btn btn-default" name="signup">Sign Up</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	
</body>
</html>