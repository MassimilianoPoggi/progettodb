<?php 
include 'header.php';
include 'functions.php';
session_start();

if (isset($_SESSION['id']))
	header("location: /home.php");

if (isset($_GET['success'])) 
	echo '<div class="alert alert-success" role="alert">Registrazione avvenuta con successo!</div>';

if (isset($_POST['login'])) {
	if(isset($_POST['user'])&&isset($_POST['psw'])&&isset($_POST['livello'])&&$_POST['user']!=""&&$_POST['psw']!=""){
	login($_POST['user'], hash('sha256',$_POST['psw']),$_POST['livello']);
	}
	else
	echo '<div class="alert alert-danger" role="alert">Rempi tutti i campi !</div>';
}

?>
	<title>FoodExpress&trade;</title>

</head>
<body>
<h1 style="text-align:center">FoodExpress&trade;</h1>
	<div class="container">
		<div class="pull-right" style="margin-top:25px">
			<a href="signup.php">Sign Up</a>
		</div>
		<div class="col-md-6 col-md-offset-3">
			<form class="form-horizontal" style="margin-top:75px" method="post">
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-3 control-label">Username </label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="inputEmail3" placeholder="user" name="user">
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-3 control-label">Password </label>
					<div class="col-sm-6">
						<input type="password" class="form-control" id="inputPassword3" placeholder="Password" name="psw">
					</div>
				</div>
				<div class="form-group">
					<label for="lista_input" class="col-sm-3 control-label">Tipo di accesso:</label>
					<div class="col-sm-6">
						<select id = "lista_input" class="form-control " name="livello">
								<option value="1">Cliente</option>
								<option value="5">Ristoratore</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-5 col-sm-10">
						<button type="submit" class="btn btn-default" name="login">Log In</button>
					</div>
				</div>
			</form>
		</div>

	</div>
	
</body>
</html>