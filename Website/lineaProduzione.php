<?php 
include 'header.php';
include 'functions.php';
check_login();
include 'navbar.php';
escapeNormalUser();	

if(isset($_POST['coda'])){
	$arrayPreparazioni=getPreparazioneLinea($_POST['coda']);
}

if(isset($_GET['id'])&&isset($_GET['update'])){
	aggiornaStatoPreparazione($_GET['id'],$_GET['update']);
	header("location: ".strtok($_SERVER['PHP_SELF'], '?'));
}

$linee_preparazione= getLineePreparazione();

?>
<title>Admin</title>
</head>

<body>
	<h1 style="text-align:center;">linee</h1>
	
	<div class="container">
	<?php 
		echo '<form class="form-horizontal" style="margin-top:75px" href='.$_SERVER['PHP_SELF'].' method="post" enctype="multipart/form-data">
				<div class="col-md-6" >
							<div class="form-group">
					<label>Lista</label>
					<select class="form-control" name="coda">
						';

		foreach ($linee_preparazione as $linea) {
				if(isset($_POST['coda'])){
					if($linea[0] == $_POST['coda'])
						echo '<option value="'.$linea[0].'"  selected="selected">'.$linea[0].'</option>';
					else
						echo '<option value="'.$linea[0].'">'.$linea[0].'</option>';
				}
				else
					echo '<option value="'.$linea[0].'">'.$linea[0].'</option>';
		}
		echo '
					</select>
				</div>';
			echo '<button class="btn btn-primary btn-sm active" >Visiona coda</button>';
			echo '</form>

				</div>';
	

		
	
			
		echo '<div class="col-md-6" >';
				if(isset($arrayPreparazioni)){
				printPreparazioneLinea($arrayPreparazioni);
	}
			echo '</div>';

	?>

	</div>


	</body>
