<?php 
include 'header.php';
include 'functions.php';
check_login();
include 'navbar.php'; 
escapeNormalUser();

$arrayCategoria=getCategorie();

if(isset($_POST['submitCategoria'])){
	if(isset($_POST['categoria'])&& $_POST['categoria']!=""){
		creaCategoria($_POST['categoria']);
	}
	else{
		echo ' <div class="alert alert-danger" role="alert">Errore, inserisci un nome.</div>';
	}
}

if (isset($_POST['submit'])) {

	$errors= array();
	$file_name = $_FILES['image']['name'];
	$file_size =$_FILES['image']['size'];
	$file_tmp =$_FILES['image']['tmp_name'];
	$file_type=$_FILES['image']['type'];
	$temp=explode('.',$_FILES['image']['name']);
	$img = end($temp);
	$file_ext=strtolower($img);
	$expensions= array("jpeg","jpg","png");
	if(in_array($file_ext,$expensions)=== false){
		$errors[]="extension not allowed, please choose a JPEG or PNG file.";
	}   
	if($file_size > 2097152){
		$errors[]='File size must be excately 2 MB';
	}
	if(empty($errors)==true){
		move_uploaded_file($file_tmp,"img/".sha1($file_name));
		//echo "Success";
	}else{
		echo ' <div class="alert alert-danger" role="alert">Formato non supportato</div>';
	}

	if ($_POST['titolo'] == "" OR $_POST['complessità'] == "" OR $_POST['categoria'] == "") { 
		echo ' <div class="alert alert-danger" role="alert">Campi vuoti !!</div>';
	 }
	else{
		creaPiatto($_POST['titolo'], "img/".sha1($file_name), $_POST['descrizione'], $_POST['tempo'], $_POST['complessità'],$_POST['categoria']);
	}
}

?>
<title>Admin</title>
</head>

<body>
	<div class="container">
		<h1 style="text-align:center;">Aggiungi una categoria</h1>
		<div class="col-md-6 col-md-offset-3">
			<form class="form-horizontal" style="margin-top:75px" method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label>Nome categoria</label>
					<input type="text" class="form-control" placeholder="Nome categoria" name="categoria">
				</div>
			<button type="submit" name="submitCategoria" class="btn btn-primary btn-lg active">Aggiungi categoria</button>
		</form>
	</div>
</div>
	<div class="container">
		<h1 style="text-align:center;">Aggiungi un piatto</h1>
		<div class="col-md-6 col-md-offset-3">
			<form class="form-horizontal" style="margin-top:75px" method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label>Nome Piatto</label>
					<input type="text" class="form-control" placeholder="Nome Piatto" name="titolo">
				</div>
				<div class="form-group">
					<label>Descrizione</label>
					<textarea type="text" class="form-control" placeholder="" name="descrizione">
					</textarea>
				</div>
				<div class="form-group">
					<label>Tempo di preparazione</label>
					<input type="number" class="form-control" placeholder="0" name="tempo">
				</div>
				<div class="form-group">
					<label>Complessità</label>
					<select class="form-control" value="1" name="complessità">
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
					</select>
			</div>
			<div class="form-group">
					<label>Categoria</label>
					<select class="form-control" value="1" name="categoria">
						
						<?php 
							
								 foreach($arrayCategoria as $row){
									 echo '<option value="'.$row['nome'].'">'.$row['nome'].'</option>';
								 }
							?>
							
				</div>
			<div class="form-group">
				<label for="exampleInputFile">Immagine</label>
				<input type="file" id="exampleInputFile" name="image">
				<p class="help-block">Inserisci un'immagine del piatto (si consiglia una dimensione di 320x320).</p>
			</div>
			<button type="submit" name="submit" class="btn btn-primary btn-lg active">Invia</button>
		</form>
	</div>
</div>
</body>
