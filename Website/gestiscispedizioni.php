<?php 
include 'header.php';
include 'functions.php';
check_login();
include 'navbar.php';
escapeNormalUser();
if(isset($_POST['coda'])){
	$arrayPreparazioni=getPreparazioneLinea($_POST['coda']);

}


if(isset($_GET['id'])&&isset($_GET['update'])){
	aggiornaStatoOrdine($_GET['id'],$_GET['update']);
	header("location: ".strtok($_SERVER['PHP_SELF'], '?'));
}



$ordini=  getTuttiOrdini();

?>
<title>Gestisci la consegna delle tue spedizioni</title>
</head>

	<body>
		<div class="container">
		<div class="row">
		<h2 class ="col-md-6 col-md-offset-3">I tuoi ordini</h2>
		<hr>
		<?php
			echo '
			<div class="col-md-6 col-md-offset-2">';
				
				echo '<p>Puoi visionare gli ordini da completare.</p>';
				if(count($ordini) == 0){ 
					echo '<div class="alert" role="alert">Storico ordini vuoto.</div>';
				 } else {
					printOrdiniRistoratore($ordini,array("In attesa","In preparazione","Pronto","In consegna"));
				} 
		echo "</div>";

		

		
		?>
		</div>
		
		<?php

		if(isset($_GET['fattura'])){
			echo '<div class="row">
		<h2 class ="col-md-6 col-md-offset-3">La fattura per l\'ordine n°'.$_GET['fattura'].' </h2>
		<hr>';
			printFattura($_GET['fattura']);
			echo '</div>';
		}
		if(isset($_GET['info'])){
			echo '<div class="row">
		<h2 class ="col-md-6 col-md-offset-3">L\' ordine : '.$_GET['info'].'</h2>
		<hr>';
			printPreparazioniStates($_GET['info']);
			echo '</div>';
		}
		
		?>
		

	</div>
	</body>
</html>